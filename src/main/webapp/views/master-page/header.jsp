<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
	<img src="images/logo.png" alt="Acme-Gym Co., Inc." />
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize access="hasRole('ADMINISTRATOR')">
			<li><a class="fNiv" href="administrator/edit.do"><spring:message code="master.page.edit"/></a></li>
			<li><a class="fNiv" href="managerUri/all.do"><spring:message code="master.page.managers"/></a></li>
		</security:authorize>
		
		<security:authorize access="hasRole('TRAINER')">
			<li><a class="fNiv" href="trainer/edit.do"><spring:message code="master.page.edit"/></a></li>
		</security:authorize>
		
		<security:authorize access="hasRole('CUSTOMER')">
			<li><a class="fNiv" href="customer/edit.do"><spring:message code="master.page.edit"/></a></li>
		</security:authorize>
		
		<security:authorize access="hasRole('MANAGER')">
			<li><a class="fNiv" href="managerUri/trainer/create.do"><spring:message code="master.page.registerATrainer"/></a></li>
			<li><a class="fNiv" href="managerUri/trainer/all.do"><spring:message code="master.page.trainers"/></a></li>
			<li><a class="fNiv" href="gym/manager/myGyms.do"><spring:message code="master.page.myGyms"/></a></li>
			<li><a class="fNiv" href="managerUri/edit.do"><spring:message code="master.page.edit"/></a></li>
		</security:authorize>
		
		
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a></li>
			<li><a class="fNiv" href="customer/create.do"><spring:message code="master.page.registerAsCustomer"/></a></li>
			<li><a class="fNiv" href="managerUri/create.do"><spring:message code="master.page.registerAsManager"/></a></li>
		</security:authorize>
		
		<security:authorize access="hasRole('ADMINISTRATOR')">
					<li><a class="fNiv" href="administrator/dashboard.do"><spring:message code="master.page.dashboard" /></a></li>
		</security:authorize>
		<li><a class="fNiv" href="gym/list.do"><spring:message code="master.page.gyms" /></a></li>
		<li><a class="fNiv" href="activity/list.do"><spring:message code="master.page.activities" /></a></li>
		
		<security:authorize access="isAuthenticated()">
			<li>
				<a class="fNiv"> 
					<spring:message code="master.page.profile" /> 
			        (<security:authentication property="principal.username" />)
				</a>
				<ul>
					<li class="arrow"></li>				
					<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
				</ul>
			</li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

