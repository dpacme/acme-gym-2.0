<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="trainer/edit.do" modelAttribute="trainer">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="userAccount" />

	<!-- Campos obligatorios a rellenar -->

	<b><spring:message code="trainer.PersonalData" /></b>

	<br />

	<acme:textbox code="trainer.name" path="name" />

	<acme:textbox code="trainer.surname" path="surname" />

	<acme:textbox code="trainer.email" path="email" />
	
	<div style="overflow: hidden">
		<div class="inline">
	<acme:textbox code="trainer.phone" path="phone" placeholder="+XX (YYY) ZZZZ"/>
		</div>
		<jstl:if test="${phone != null}">
			<div>
				<span class="message"><spring:message code="${phone}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="trainer.postalAddress" path="postalAddress"
				placeholder="Ej: 41010" />
		</div>
		<jstl:if test="${postal != null}">
			<div>
				<span class="message"><spring:message code="${postal}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />
	
	
	<acme:textbox code="trainer.city" path="city" />
	
	<acme:textbox code="trainer.country" path="country" />

	<br />
	
	

	<!-- Acciones -->
	
	<acme:submit name="save" code="trainer.save"/>
	
	<acme:cancel url="" code="trainer.cancel"/>

</form:form>

<br>

