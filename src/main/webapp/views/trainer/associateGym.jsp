<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="${requestURI}" modelAttribute="trainer">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="name" />
	<form:hidden path="surname" />
	<form:hidden path="email" />
	<form:hidden path="phone" />
	<form:hidden path="postalAddress" />
	<form:hidden path="city" />
	<form:hidden path="country" />
	<form:hidden path="userAccount" />
	<form:hidden path="activities" />


	<acme:select items="${gyms}" itemLabel="name" code="trainer.gyms" path="gym"/>
	
	<br/>
	<!-- Acciones -->
	<acme:submit name="save" code="trainer.save"/>
	
	<acme:cancel url="managerUri/trainer/all.do" code="trainer.cancel"/>

</form:form>

<br>
