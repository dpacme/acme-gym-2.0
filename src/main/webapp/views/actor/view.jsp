<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<div class="actor" style="text-align: center">
	<b><spring:message code="actor.name" />:</b> ${actor.name}<br>
	<p />
	<b><spring:message code="actor.surname" />:</b> ${actor.surname}<br>
	<p />
	<b><spring:message code="actor.email" />:</b> ${actor.email}<br>
	<p />
	<b><spring:message code="actor.phone" />:</b> ${actor.phone}<br>
	<p />
	<b><spring:message code="actor.postalAddress" />:</b> ${actor.postalAddress}<br>
	<p />
	<b><spring:message code="actor.city" />:</b> ${actor.city}<br>
	<p />
	<b><spring:message code="actor.country" />:</b> ${actor.country}<br>
	<p />
</div>
