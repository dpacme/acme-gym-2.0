<%@page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<script>
$(function() {
   
    $('.datetimepicker').timepicker();
    
  });
</script>

<form:form action="activity/manager/edit.do" modelAttribute="activity">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="gym" />
	<form:hidden path="isCanceled" />
	
	<acme:textbox code="activity.title" path="title" />
	<acme:textbox code="activity.description" path="description" />
	<form:label path="dayWeek">
        <spring:message code="activity.dayWeek" />
    </form:label><b></b>
	<form:select path="dayWeek" style="margin-bottom: 10px">
		<form:option value="Monday"><spring:message code="activity.dayWeekMonday"/></form:option>
		<form:option value="Tuesday"><spring:message code="activity.dayWeekTuesday"/></form:option>
		<form:option value="Wednesday"><spring:message code="activity.dayWeekWednesday"/></form:option>
		<form:option value="Thursday"><spring:message code="activity.dayWeekThursday"/></form:option>
		<form:option value="Friday"><spring:message code="activity.dayWeekFriday"/></form:option>
		<form:option value="Saturday"><spring:message code="activity.dayWeekSaturday"/></form:option>
		<form:option value="Sunday"><spring:message code="activity.dayWeekSunday"/></form:option>
	</form:select>
	<form:errors class="error" path="dayWeek" />
	<br/>
	<spring:message code="activity.date.click" var="datePlaceholder"/>
	<spring:message code="activity.startTime" />:
	<form:input type="text" id="startTime" path="startTime" readonly="readonly" placeholder="${datePlaceholder}" class="datetimepicker"/>
	<form:errors class="error" path="startTime" />	
	<br />
	
	<spring:message code="activity.endTime" />:
	<form:input type="text" id="endTime" path="endTime" readonly="readonly" placeholder="${datePlaceholder}" class="datetimepicker"/>
	<form:errors class="error" path="endTime" />	
	<br />
	
	<form:label path="seatsAvailable">
        <spring:message code="activity.seatsAvailable" />
    </form:label><b></b>
    <form:input type="number" path="seatsAvailable"/>
    <form:errors class="error" path="seatsAvailable" />	
	<br>
	<b><spring:message code="activity.picturesW" /></b>
	<br>
	<form:label path="pictures">
        <spring:message code="activity.pictures" />
    </form:label><b></b>
    <form:input type="url" path="pictures" />
    <form:errors class="error" path="pictures" />	
	<br>

	<input type="submit" name="save" value="<spring:message code="activity.save" />" />&nbsp; 

	<jstl:if test="${activity.id != 0}">
		<input type="submit" name="delete" value="<spring:message code="activity.delete" />"
		onclick="return confirm('<spring:message code="activity.confirm.delete" />')" />&nbsp;
	</jstl:if>

	<input type="button" name="cancel"
		value="<spring:message code="activity.cancel" />"
		onclick="javascript: window.location.replace('gym/listActivities.do?gymId=${activity.gym.id}');" />
<br />

</form:form>

