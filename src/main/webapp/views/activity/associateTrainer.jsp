<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="${requestURI}" modelAttribute="activity">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="title" />
	<form:hidden path="pictures" />
	<form:hidden path="description" />
	<form:hidden path="dayWeek" />
	<form:hidden path="startTime" />
	<form:hidden path="endTime" />
	<form:hidden path="seatsAvailable" />
	<form:hidden path="isCanceled" />
	<form:hidden path="gym" />
	<form:hidden path="customers" />


	<acme:select items="${trainers}" itemLabel="name" code="activity.trainers" path="trainers"/>
	
	<br/>
	<spring:message code="activity.helpAddTrainer" />
	<br/>
	<!-- Acciones -->
	<acme:submit name="save" code="customer.signIn"/>
	
	<acme:cancel url="gym/listActivities.do?gymId=${gymId}" code="customer.cancel"/>

</form:form>

<br>
