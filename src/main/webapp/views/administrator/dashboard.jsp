<%--
 * action-1.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<security:authorize access="hasRole('ADMINISTRATOR')">

	<h2>
		<spring:message code="administrator.minMaxAvgAndStadevOfGymsPerManager" />
	</h2>
	<jstl:out value="${minGymsPerManager}" />
	<br>
	<jstl:out value="${maxGymsPerManager}" />
	<br>
	<jstl:out value="${avgGymsPerManager}" />
	<br>
	<jstl:out value="${stdevGymsPerManager}" />
	<br>

	<h2>
		<spring:message code="administrator.minMaxAvgStdDevNumberGymsPerCustomer" />
	</h2>
	
	<jstl:out value="${minNumberGymsPerCustomer}" />
	<br>
	<jstl:out value="${maxNumberGymsPerCustomer}" />
	<br>
	<jstl:out value="${avgNumberGymsPerCustomer}" />
	<br>
	<jstl:out value="${stdevNumberGymsPerCustomer}" />
	<br>
	
	<h2>
		<spring:message code="administrator.minMaxAvgAndStadevOfCustomersPerGym" />
	</h2>
	
	<jstl:out value="${minCustomersPerGym}" />
	<br>
	<jstl:out value="${maxCustomersPerGym}" />
	<br>
	<jstl:out value="${avgCustomersPerGym}" />
	<br>
	<jstl:out value="${stadevCustomersPerGym}" />
	<br>
	
	<h2>
		<spring:message code="administrator.getGymsWithMoreActivities" />
	</h2>
	<display:table pagesize="5" class="displaytag" keepStatus="true" name="getGymsWithMoreActivities" requestURI="${requestURI}" id="row">

<!-- Attributes -->

	<spring:message code="gym.logo" var="logo" />
	<display:column title="${logo}">
	<img src="${row.logo}" height="64" width="64">
 	</display:column>
	    
	<spring:message code="gym.address" var="address" />
	<display:column property="address" title="${address}" sortable="true"/>
	
	<spring:message code="gym.name" var="name" />
	<display:column property="name" title="${name}" sortable="true"/>
	
	<spring:message code="gym.fee" var="fee" />
	<display:column property="fee" title="${fee}" sortable="true"/>
	
		<% int i=0;%>
	<jstl:forEach var="activity" items="${row.activities}">
		<jstl:if test="${activity.isCanceled!=true}">
		<% i++; %> 
		</jstl:if>
	</jstl:forEach>
	<spring:message code="gym.activities" var="activities" />
	<display:column title="${activities}">
	<%= i %>
	</display:column>
	
</display:table>

</security:authorize>

<h2>
		<spring:message code="administrator.getCustomerWithMoreActivities" />
</h2>

<display:table name="getCustomerWithMoreActivities" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="customer.name" var="name" />
	<display:column property="name" title="${name}" sortable="true"/>
	
	<spring:message code="customer.surname" var="surname" />
	<display:column property="surname" title="${surname}" sortable="true"/>
	
	<spring:message code="customer.email" var="email" />
	<display:column property="email" title="${email}" sortable="true"/>
	
	<spring:message code="customer.phone" var="phone" />
	<display:column property="phone" title="${phone}" sortable="true"/>
	
	<spring:message code="customer.postalAddress" var="postalAddress" />
	<display:column property="postalAddress" title="${postalAddress}" sortable="true"/>
	
	<spring:message code="customer.city" var="city" />
	<display:column property="city" title="${city}" sortable="true"/>
	
	<spring:message code="customer.country" var="country" />
	<display:column property="country" title="${country}" sortable="true"/>
	
	<spring:message code="customer.activities" var="activities" />
		<display:column title="${activities}">
		${fn:length(row.activities)}
		</display:column>
</display:table>

<h2>
		<spring:message code="administrator.minMaxAvgOfStepsPerWorkout" />
	</h2>
	
	<jstl:out value="${minStepsPerWorkout}" />
	<br>
	<jstl:out value="${maxStepsPerWorkout}" />
	<br>
	<jstl:out value="${avgStepsPerWorkout}" />
<br>
	
<h2>
	<spring:message code="administrator.minMaxAvgOfWorkoutsPerGym" />
</h2>

<jstl:out value="${minWorkoutsPerGym}" />
<br>
<jstl:out value="${maxWorkoutsPerGym}" />
<br>
<jstl:out value="${avgWorkoutsPerGym}" />
<br>


<h2>
		<spring:message code="administrator.getWorkoutsOrderByNumberOfStars" />
</h2>

<display:table name="getWorkoutsOrderByNumberOfStars" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="workout.title" var="title" />
	<display:column property="title" title="${title}" sortable="true" />

	<spring:message code="workout.description" var="description" />
	<display:column property="description" title="${description}"
		sortable="true" />

	<spring:message code="workout.details" var="detailsHeader" />
	<display:column title="${detailsHeader}">
		<acme:button href="workout/showDisplay.do?workoutId=${row.id}"
			name="showGym" code="workout.show" />
	</display:column>

	<spring:message code="workout.gym" var="showGymHeader" />
	<display:column title="${showGymHeader}">

		<acme:button href="gym/showByWorkout.do?workoutID=${row.id}"
			name="showGym" code="workout.see" />
	</display:column>

	<jsp:useBean id="loginService" class="security.LoginService"
		scope="page" />




</display:table>