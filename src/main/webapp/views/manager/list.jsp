<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="managers" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="manager.name" var="name" />
	<display:column property="name" title="${name}" sortable="true"/>
	
	<spring:message code="manager.surname" var="surname" />
	<display:column property="surname" title="${surname}" sortable="true"/>
	
	<spring:message code="manager.email" var="email" />
	<display:column property="email" title="${email}" sortable="true"/>
	
	<spring:message code="manager.phone" var="phone" />
	<display:column property="phone" title="${phone}" sortable="true"/>
	
	<spring:message code="manager.postalAddress" var="postalAddress" />
	<display:column property="postalAddress" title="${postalAddress}" sortable="true"/>
	
	<spring:message code="manager.city" var="city" />
	<display:column property="city" title="${city}" sortable="true"/>
	
	<spring:message code="manager.country" var="country" />
	<display:column property="country" title="${country}" sortable="true"/>
	
	<display:column>
		<jstl:if test="${row.getIsBanned() != true}">
			<acme:button href="administrator/manager/ban.do?managerId=${row.id}"
				name="banManager" code="manager.ban" />
		</jstl:if>
			
		<jstl:if test="${row.getIsBanned() == true}">
			<acme:button href="administrator/manager/unban.do?managerId=${row.id}"
				name="unbanManager" code="manager.unban" />
		</jstl:if>
			
	</display:column>
	
</display:table>

<br/>