<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
	
<display:table name="annotations" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="annotation.comment" var="comment" />
	<display:column property="comment" title="${comment}" sortable="true" />

	<spring:message code="annotation.numberStars" var="numberStars" />
	<display:column property="numberStars" title="${numberStars}"
		sortable="true" />
		
	<spring:message code="annotation.actor" var="actor" />
	<display:column title="${actor}">
		${row.getActor().getName()}&nbsp;${row.getActor().getSurname()}
	</display:column>
		
	<jsp:useBean id="loginService" class="security.LoginService"
		scope="page" />
	<security:authorize access="isAuthenticated()">
		<display:column>
			<jstl:if test="${row.getActor().getUserAccount().getId() == loginService.getPrincipal().getId()}">
				<acme:button href="annotation/edit.do?annotationID=${row.id}"
					name="editAnnotation" code="annotation.edit" />
			</jstl:if>
		</display:column>
	</security:authorize>
	

</display:table>

<br />

<security:authorize access="isAuthenticated()">

	<a href="annotation/create.do?workoutID=${workout.getId()}"><spring:message
			code="annotation.create" /></a>

</security:authorize>