<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="${requestURI}" modelAttribute="annotation">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="actor" />
	<form:hidden path="workout" />
	

	<acme:textbox code="annotation.comment" path="comment" />
	<form:label path="numberStars">
        <spring:message code="annotation.numberStars" />
    </form:label><b></b>
    <form:input type="number" path="numberStars"/>
    <form:errors class="error" path="numberStars" />	
	<br>
	
	<input type="submit" name="save"
		value="<spring:message code="annotation.save" />" />
		
	<input type="button" name="cancel"
	value="<spring:message code="annotation.cancel" />"
	onclick="javascript: window.location.replace('annotation/list.do?workoutID=${annotation.getWorkout().getId()}');" />
	<br />

</form:form>

<br>

