<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<div>
	<ul>
		<li>
			<b><spring:message code="gym.logo"/>:</b>
			${gym.logo}
		</li>
		<li>
			<b><spring:message code="gym.address"/>:</b>
			${gym.address}
		</li>
		<li>
			<b><spring:message code="gym.name"/>:</b>
			${gym.name}
		</li>
		<li>
			<b><spring:message code="gym.fee"/>:</b>
			${gym.fee}
		</li>
	</ul>
</div>

<br/>

<b><spring:message code="gym.listTrainers"/></b>
<acme:button href="gym/listTrainers.do?gymId=${gym.id}" name="listGym"
			code="gym.listTrainers.see" />

<br/>

<b><spring:message code="gym.listActivities"/></b>
<acme:button href="gym/listActivities.do?gymId=${gym.id}"
			name="listGym" code="gym.listActivities.see" />
			
<br/>

<b><spring:message code="gym.listWorkouts"/></b>
<acme:button href="workout/listByGym.do?gymID=${gym.id}"
			name="listGym" code="gym.listActivities.see" />