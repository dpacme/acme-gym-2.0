<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="utilidades" uri="/WEB-INF/tld/utilidades.tld"%>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

<security:authorize access="hasRole('MANAGER')">

	<jstl:if test="${create}">
		<div>
			<H5>
				<a href="gym/manager/create.do"> <spring:message
						code="gym.create" />
				</a>
			</H5>
		</div>
	</jstl:if>
</security:authorize>

<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="gyms" requestURI="${requestURI}" id="row">

	<!-- Attributes -->

	<security:authorize access="hasRole('MANAGER')">
		<jstl:if test="${create && row.manager.id == idLogged}">
			<display:column>
				<a href="gym/manager/edit.do?gymId=${row.id}"> <spring:message
						code="gym.edit" />
				</a>
			</display:column>
		</jstl:if>
	</security:authorize>

	<spring:message code="gym.logo" var="logo" />
	<display:column title="${logo}">
		<img src="${row.logo}" height="64" width="64">
	</display:column>

	<spring:message code="gym.address" var="address" />
	<display:column property="address" title="${address}" sortable="true" />

	<spring:message code="gym.name" var="name" />
	<display:column property="name" title="${name}" sortable="true" />

	<spring:message code="gym.fee" var="fee" />
	<display:column property="fee" title="${fee}" sortable="true" />

	<spring:message code="gym.details" var="details" />
	<display:column title="${details}" sortable="true" >
		<div>
			<input type="button" name="${showGym}" value="<spring:message code="gym.details.see"/>"
				onclick="javascript: window.location.replace('gym/show.do?gymId=${row.id}');" />
		</div>
	</display:column>
	

	<spring:message code="gym.listTrainers" var="listTrainersHeader" />
	<display:column title="${listTrainersHeader}">

		<acme:button href="gym/listTrainers.do?gymId=${row.id}" name="listGym"
			code="gym.listTrainers.see" />
	</display:column>

	<spring:message code="gym.listActivities" var="listActivitiesHeader" />
	<display:column title="${listActivitiesHeader}">

		<acme:button href="gym/listActivities.do?gymId=${row.id}"
			name="listGym" code="gym.listActivities.see" />

	</display:column>

	<security:authorize access="hasRole('CUSTOMER')">
		<spring:message code="gym.customer.actions" var="actionsHeader" />
		<display:column title="${actionsHeader}">
			<jstl:choose>
				<jstl:when
					test="${utilidades:listCustomerContainsUserAccount(row.customers, loginService.getPrincipal()) == false}">
					<div class="inline">
						<acme:button href="gym/customer/joinGym.do?gymId=${row.id}"
							name="join" code="gym.customer.actions.join" />
					</div>
					<div class="inline">
						<input type="button" href="#" disabled="disabled" name="leave"
							value='<spring:message code="gym.customer.actions.leave" />' />
					</div>
				</jstl:when>
				<jstl:otherwise>
					<div class="inline">
						<input type="button" href="#" disabled="disabled" name="ban"
							value='<spring:message code="gym.customer.actions.join" />' />
					</div>
					<div class="inline">
						<acme:button href="gym/customer/leaveGym.do?gymId=${row.id}"
							name="unban" code="gym.customer.actions.leave" />
					</div>
				</jstl:otherwise>
			</jstl:choose>
		</display:column>
	</security:authorize>
	
	<spring:message code="gym.listWorkouts" var="listWorkoutsHeader" />
	<display:column title="${listWorkoutsHeader}">

		<acme:button href="workout/listByGym.do?gymID=${row.id}"
			name="listWorkout" code="gym.listActivities.see" />

	</display:column>

</display:table>
