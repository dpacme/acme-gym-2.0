<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<div>
	<ul>
		<li><b><spring:message code="workout.title" />:</b>
			${workout.title}</li>
		<li><b><spring:message code="workout.description" />:</b>
			${workout.description}</li>
	</ul>
</div>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

<security:authorize access="hasRole('MANAGER')">
	<jstl:if
		test="${workout.getGym().getManager().getUserAccount().getId() == loginService.getPrincipal().getId()}">
		<acme:button href="workout/manager/edit.do?workoutID=${workout.id}"
			name="editWorkout" code="workout.edit" />
	</jstl:if>
</security:authorize>

<h2>
	<spring:message code="workout.steps" />
</h2>

<display:table name="steps" id="row" requestURI="${requestURI}"
	class="displaytag">

	<spring:message code="step.description" var="description" />
	<display:column property="description" title="${description}"
		sortable="true" />

	<spring:message code="step.tutorial" var="tutorial" />
	<display:column title="${tutorial}" sortable="false">
			<jstl:if test="${!row.tutorial.isEmpty()}">
				<iframe src="${row.tutorial}"></iframe>
			</jstl:if>
	</display:column>

	<security:authorize access="hasRole('MANAGER')">
		<display:column>
			<jstl:if
				test="${row.getWorkout().getGym().getManager().getUserAccount().getId() == loginService.getPrincipal().getId()}">
				<acme:button href="step/manager/edit.do?stepID=${row.id}"
					name="editStep" code="step.edit" />
			</jstl:if>
		</display:column>
	</security:authorize>
</display:table>

<security:authorize access="hasRole('MANAGER')">

	<jstl:if
		test="${workout.getGym().getManager().getUserAccount().getId() == loginService.getPrincipal().getId()}">
		<acme:button href="step/manager/create.do?workoutID=${workout.id}"
			name="createStep" code="workout.moreSteps" />
	</jstl:if>

</security:authorize>


<h2>
	<spring:message code="workout.annotations" />
</h2>

<display:table name="annotations" id="row" requestURI="${requestURI}"
	class="displaytag">

	<spring:message code="annotation.comment" var="comment" />
	<display:column property="comment" title="${comment}" sortable="true" />

	<spring:message code="annotation.numberStars" var="numberStars" />
	<display:column property="numberStars" title="${numberStars}"
		sortable="true" />
		
	<spring:message code="annotation.actor" var="actor" />
	<display:column title="${actor}">
		${row.getActor().getName()}&nbsp;${row.getActor().getSurname()}
	</display:column>


</display:table>

<security:authorize access="isAuthenticated()">

	<a href="annotation/create.do?workoutID=${workout.getId()}"><spring:message
			code="annotation.create" /></a>

</security:authorize>
