<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="${requestURI}" modelAttribute="workoutForm">
	
	<b><spring:message code="workout.workoutData" /></b>

	<br />

	<acme:textbox code="workout.title" path="title" />
	<acme:textbox code="workout.description" path="descriptionWorkout" />
	<acme:select items="${gyms}" itemLabel="name" code="workout.gym" path="gym"/>
	<br>
	
	<b><spring:message code="workout.firstStep" /></b>

	<br />
	
	<acme:textbox code="workout.description" path="descriptionStep" />
	<acme:textbox code="workout.tutorial" path="tutorial" />
	<br>
	
	<input type="submit" name="next"
		value="<spring:message code="workout.moreSteps" />" />&nbsp; 
		
	<input type="submit" name="save"
		value="<spring:message code="workout.save" />" />&nbsp;
		
	<input type="button" name="cancel"
		value="<spring:message code="workout.cancel" />"
		onclick="javascript: window.location.replace('gym/manager/myGyms.do');" /> <br />

</form:form>

<br>

