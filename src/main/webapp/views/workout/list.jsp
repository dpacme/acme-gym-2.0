<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<fieldset style="width: 20%">
			<legend>
				<spring:message code="workout.search" />
			</legend>
			<form method="POST" action="workout/search.do">

				<spring:message code="workout.keyword" />
				<spring:message code="workout.keyword" var="keywordPlaceholder" />
				<input type="text" id="keyword" name="keyword"
					placeholder="${keywordPlaceholder}" /> <br />
					
				<input type="hidden" id="gymId" name="gymId" value="${gym.id}" readonly="readonly" />
				
				<input type="submit" name="search"
					value="<spring:message code="workout.search"/>" />
			</form>
</fieldset>
		
<display:table name="workouts" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="workout.title" var="title" />
	<display:column property="title" title="${title}" sortable="true" />

	<spring:message code="workout.description" var="description" />
	<display:column property="description" title="${description}"
		sortable="true" />

	<spring:message code="workout.details" var="detailsHeader" />
	<display:column title="${detailsHeader}">
		<acme:button href="workout/showDisplay.do?workoutId=${row.id}"
			name="showGym" code="workout.show" />
	</display:column>

	<spring:message code="workout.gym" var="showGymHeader" />
	<display:column title="${showGymHeader}">

		<acme:button href="gym/showByWorkout.do?workoutID=${row.id}"
			name="showGym" code="workout.see" />
	</display:column>
	
	<spring:message code="workout.annotation" var="showAnnotations" />
	<display:column title="${showAnnotations}">
		<acme:button href="annotation/list.do?workoutID=${row.id}"
			name="createAnnotation" code="workout.see" />
	</display:column>

	<jsp:useBean id="loginService" class="security.LoginService"
		scope="page" />




</display:table>

<br />

<security:authorize access="hasRole('MANAGER')">

	<a href="workout/manager/create.do"><spring:message
			code="workout.create" /></a>

</security:authorize>