<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="${requestURI}" modelAttribute="workout">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="steps" />
	<form:hidden path="annotations" />
	

	<acme:textbox code="workout.title" path="title" />
	<acme:textbox code="workout.description" path="description" />
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:select items="${gyms}" itemLabel="name" code="workout.gym" path="gym"/>
		</div>
		<jstl:if test="${select != null}">
			<div>
				<span class="message"><spring:message code="${select}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />
	
	<input type="submit" name="save"
		value="<spring:message code="workout.save" />" />&nbsp;
		
	<input type="submit" name="delete"
			value="<spring:message code="workout.delete" />"/>&nbsp;
		
	<input type="button" name="cancel"
		value="<spring:message code="workout.cancel" />"
		onclick="javascript: window.location.replace('workout/showDisplay.do?workoutId=${workout.id}');" /> <br />

</form:form>

<br>

