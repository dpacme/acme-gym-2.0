<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<display:table name="steps" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="step.description" var="description" />
	<display:column property="description" title="${description}" sortable="true"/>
	
	<spring:message code="step.tutorial" var="tutorial" />
	<display:column property="tutorial" title="${tutorial}" sortable="true"/>
	
	<spring:message code="step.workout" var="workoutHeader" />
	<display:column title="${workoutHeader}">
		<acme:button href="workout/manager/listByStep.do?stepID=${row.id}" name="listStep"
			code="step.see" />
	</display:column>
	
	<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />
	
	<security:authorize access="hasRole('MANAGER')">
		<display:column>
			<jstl:if test="${row.getWorkout().getGym().getManager().getUserAccount().getId() == loginService.getPrincipal().getId()}">
				<acme:button href="step/manager/edit.do?stepID=${row.id}" name="editStep"
					code="step.edit" />
			</jstl:if>
		</display:column>
	</security:authorize>
	
</display:table>

<br/>