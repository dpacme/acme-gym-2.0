<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="${requestURI}" modelAttribute="step">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="workout" />

	<acme:textbox code="step.description" path="description" />
	<acme:textbox code="step.tutorial" path="tutorial" />
	
	<br>

	<jstl:if test="${requestURI.contains('create')}">
		<input type="submit" name="next"
			value="<spring:message code="step.moreSteps" />" />&nbsp; 
	</jstl:if>
		
	<input type="submit" name="save"
		value="<spring:message code="step.save" />" />&nbsp;
	
	<jstl:if test="${requestURI.contains('edit')}">
		<input type="submit" name="delete"
			value="<spring:message code="step.delete" />" />&nbsp; 
	</jstl:if>
		
	<input type="button" name="cancel"
		value="<spring:message code="step.cancel" />"
		onclick="javascript: window.location.replace('workout/showDisplay.do?workoutId=${step.workout.id}');" /> <br />
		
		

</form:form>

