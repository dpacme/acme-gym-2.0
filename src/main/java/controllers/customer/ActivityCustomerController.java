/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Activity;
import domain.Customer;
import services.ActivityService;
import services.CustomerService;

@Controller
@RequestMapping("/activity/customer")
public class ActivityCustomerController extends AbstractController {

	// Services

	@Autowired
	private ActivityService activityService;

	@Autowired
	private CustomerService	customerService;


	// Constructors -----------------------------------------------------------

	public ActivityCustomerController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	// List Method ------------------------------------------------

	@RequestMapping(value = "/joinActivity", method = RequestMethod.GET)
	public ModelAndView joinActivity(@RequestParam Integer activityId) {
		ModelAndView result;

		Customer customer = customerService.findByPrincipal();
		Activity activity = activityService.findOne(activityId);

		boolean tienePermiso = false;

		if (customer != null && activity != null && !activity.getIsCanceled()) {
			if (activity.getGym().getCustomers().contains(customer)) {
				tienePermiso = true;
			}
		}

		if (tienePermiso) {

			boolean exito = this.activityService.joinActivity(activityId);

			if (exito) {
				result = new ModelAndView("redirect:/activity/list.do");
			} else {
				result = new ModelAndView("misc/error");
				result.addObject("codigoError", "error.authorization");
			}
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/leaveActivity", method = RequestMethod.GET)
	public ModelAndView leaveActivity(@RequestParam Integer activityId) {
		ModelAndView result;

		boolean tienePermiso = false;

		Customer customer = customerService.findByPrincipal();
		Activity activity = activityService.findOne(activityId);

		if (customer != null && activity != null && !activity.getIsCanceled()) {
			if (activity.getGym().getCustomers().contains(customer)) {
				tienePermiso = true;
			}
		}

		if (tienePermiso) {


			boolean exito = this.activityService.leaveActivity(activityId);

			if (exito) {
				result = new ModelAndView("redirect:/activity/list.do");
			} else {
				result = new ModelAndView("misc/error");
				result.addObject("codigoError", "error.authorization");
			}
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}
}
