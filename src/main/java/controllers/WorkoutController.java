
package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Gym;
import domain.Workout;
import services.GymService;
import services.WorkoutService;

@Controller
@RequestMapping("/workout")
public class WorkoutController extends AbstractController {

	// Services

	@Autowired
	private GymService		gymService;

	@Autowired
	private WorkoutService	workoutService;


	// Constructors -----------------------------------------------------------

	public WorkoutController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// --------------- List ----------------------

	@RequestMapping(value = "/listByGym", method = RequestMethod.GET)
	public ModelAndView listByGym(@RequestParam int gymID) {
		ModelAndView result;

		Gym gym = this.gymService.findOne(gymID);

		boolean tienePermiso = true;

		if (gym != null) {
			if (gym.getIsDeleted())
				tienePermiso = false;
		} else
			tienePermiso = false;

		if (tienePermiso) {
			result = new ModelAndView("workout/listByGym");
			result.addObject("workouts", gym.getWorkouts());
			result.addObject("gym", gym);
			result.addObject("requestURI", "workout/listByGym.do");
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/showDisplay", method = RequestMethod.GET)
	public ModelAndView showDisplay(@RequestParam int workoutId) {
		ModelAndView result;

		Workout workout = this.workoutService.findOne(workoutId);
		boolean tienePermiso = true;

		if (workout != null) {
			if (workout.getGym().getIsDeleted())
				tienePermiso = false;
		} else
			tienePermiso = false;

		if (tienePermiso) {
			result = new ModelAndView("workout/showDisplay");
			result.addObject("requestURI", "workout/showDisplay.do");
			result.addObject("workout", workout);
			result.addObject("steps", workout.getSteps());
			result.addObject("annotations", workout.getAnnotations());
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;

	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, params = "search")
	public ModelAndView searchPost(String keyword, Integer gymId) {
		return this.searchWorkouts(keyword, gymId);
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET, params = "search")
	public ModelAndView searchGet(String keyword, Integer gymId) {
		return this.searchWorkouts(keyword, gymId);
	}

	private ModelAndView searchWorkouts(String keyword, Integer gymId) {
		ModelAndView result;
		final Collection<Workout> res;

		res = this.workoutService.searchWorkouts(keyword, gymId);

		result = new ModelAndView("workout/listByGym");

		if (gymId != null) {
			Gym gym = this.gymService.findOne(gymId);
			Assert.notNull(gym);
			Assert.isTrue(gym.getIsDeleted() != true);
			result.addObject("gym", gym);
		}

		result.addObject("workouts", res);
		result.addObject("requestURI", "workout/search.do");
		result.addObject("create", false);

		return result;
	}

}
