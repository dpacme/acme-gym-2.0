/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.manager;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Gym;
import domain.Manager;
import services.GymService;
import services.ManagerService;

@Controller
@RequestMapping("/gym/manager")
public class GymManagerController extends AbstractController {

	// Services

	@Autowired
	private GymService		gymService;

	@Autowired
	private ManagerService	managerService;


	// Constructors -----------------------------------------------------------

	public GymManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// Creation --------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;

		Gym gym = gymService.create();
		gym.setIsDeleted(false);
		result = createEditModelAndView(gym);

		return result;
	}

	// Edition ---------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int gymId) {
		ModelAndView result;
		Gym gym;

		Manager manager = managerService.findByPrincipal();
		Assert.notNull(manager);

		gym = gymService.findOne(gymId);
		Assert.notNull(gym);

		if (manager.getId() == gym.getManager().getId() && !gym.getIsDeleted()) {
			result = createEditModelAndView(gym);
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Gym gym, BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = createEditModelAndView(gym);
		} else {
			try {
				if (gym.getId() == 0) {
					gymService.createGym(gym.getLogo(), gym.getName(), gym.getAddress(), gym.getFee());
					result = new ModelAndView("redirect:/gym/manager/myGyms.do");
				} else {
					Manager manager = managerService.findByPrincipal();

					if (manager != null) {
						if (manager.getId() == gym.getManager().getId()) {
							if (!gym.getIsDeleted()) {
								gymService.updateGym(gym.getId(), gym.getLogo(), gym.getName(), gym.getAddress(), gym.getFee());
								result = new ModelAndView("redirect:/gym/manager/myGyms.do");
							} else {
								result = new ModelAndView("misc/error");
								result.addObject("codigoError", "error.authorization");
							}
						} else {
							result = new ModelAndView("misc/error");
							result.addObject("codigoError", "error.authorization");
						}
					} else {
						result = new ModelAndView("misc/error");
						result.addObject("codigoError", "error.authorization");
					}
				}
			} catch (Throwable oops) {
				result = createEditModelAndView(gym, "gym.commit.error");
			}
		}
		return result;
	}

	@RequestMapping(value = "/myGyms", method = RequestMethod.GET)
	public ModelAndView myGyms() {
		ModelAndView result;

		Manager manager = managerService.findByPrincipal();
		if (manager == null) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		} else {

			Collection<Gym> gyms = gymService.getGymsNotDeletedFromManager(manager.getId());

			result = new ModelAndView("gym/list");
			result.addObject("gyms", gyms);
			result.addObject("requestURI", "gym/manager/myGyms.do");
			result.addObject("create", true);
			result.addObject("idLogged", manager.getId());
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(Gym gym) {
		ModelAndView result;
		try {
			gymService.deleteGym(gym.getId());
			result = new ModelAndView("redirect:myGyms.do");
		} catch (Throwable oops) {
			result = this.createEditModelAndView(gym, "gym.commit.error");
		}

		return result;
	}

	protected ModelAndView createEditModelAndView(Gym gym) {
		ModelAndView result;

		result = createEditModelAndView(gym, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(Gym gym, String message) {
		ModelAndView result;

		result = new ModelAndView("gym/edit");
		result.addObject("gym", gym);
		result.addObject("message", message);

		return result;
	}
}
