
package controllers.manager;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Step;
import domain.Workout;
import services.StepService;
import services.WorkoutService;

@Controller
@RequestMapping("/step/manager")
public class StepManagerController extends AbstractController {

	// Services

	@Autowired
	private StepService		stepService;

	@Autowired
	private WorkoutService	workoutService;


	// Constructors -----------------------------------------------------------

	public StepManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// --------------- List ----------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam final int workoutID) {
		ModelAndView result;

		final Workout workout = this.workoutService.findOne(workoutID);

		result = new ModelAndView("step/manager/list");
		result.addObject("steps", workout.getSteps());
		result.addObject("requestURI", "step/manager/list.do");

		return result;
	}

	// --------------- Creation ----------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int workoutID) {
		ModelAndView res;
		try{
			Step step;
			final Workout workout = this.workoutService.findOne(workoutID);
			Assert.isTrue(this.workoutService.compruebaWorkout(workout));
			step = this.stepService.create(workout);

			res = new ModelAndView("step/manager/create");
			res.addObject("step", step);
			res.addObject("requestURI", "step/manager/create.do");
		}catch(Throwable oops){
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "next")
	public ModelAndView next(@Valid final Step step, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createCreateModelAndView(step);
		else
			try {
				this.stepService.cambiaTutorial(step);
				final Step result = this.stepService.saveAndFlush(step);
				res = new ModelAndView("redirect:/step/manager/create.do?workoutID=" + result.getWorkout().getId());
			} catch (final Throwable oops) {
				res = this.createCreateModelAndView(step, "step.commit.error");

			}
		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Step step, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createCreateModelAndView(step);
		else
			try {
				this.stepService.cambiaTutorial(step);
				this.stepService.save(step);
				res = new ModelAndView("redirect:/workout/showDisplay.do?workoutId=" + step.getWorkout().getId());
			} catch (final Throwable oops) {
				res = this.createCreateModelAndView(step, "gService.commit.error");

			}
		return res;
	}

	// --------------- Edition ----------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int stepID) {
		ModelAndView res;

		try{
			final Step step = this.stepService.findOne(stepID);
			Assert.isTrue(this.workoutService.compruebaWorkout(step.getWorkout()));
			res = new ModelAndView("step/manager/edit");
			res.addObject("step", step);
			res.addObject("requestURI", "step/manager/edit.do");
		}catch(Throwable oops){
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}


		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Step step, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createEditModelAndView(step);
		else
			try {
				this.stepService.cambiaTutorial(step);
				this.stepService.save(step);
				res = new ModelAndView("redirect:/workout/showDisplay.do?workoutId=" + step.getWorkout().getId());
			} catch (final Throwable oops) {
				res = this.createEditModelAndView(step, "step.commit.error");

			}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid final Step step, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createEditModelAndView(step);
		else
			try {
				this.stepService.delete(step);
				res = new ModelAndView("redirect:/workout/showDisplay.do?workoutId=" + step.getWorkout().getId());
			} catch (final Throwable oops) {
				res = this.createEditModelAndView(step, "step.commit.error");

			}
		return res;
	}

	// Ancillary methods-------------------------------------
	protected ModelAndView createCreateModelAndView(final Step step) {
		ModelAndView res;
		res = this.createCreateModelAndView(step, null);
		return res;
	}

	protected ModelAndView createCreateModelAndView(final Step step, final String message2) {
		ModelAndView res;

		res = new ModelAndView("step/manager/create");
		res.addObject("step", step);
		res.addObject("requestURI", "step/manager/create.do");
		return res;
	}

	protected ModelAndView createEditModelAndView(final Step step) {
		ModelAndView res;
		res = this.createEditModelAndView(step, null);
		return res;
	}

	protected ModelAndView createEditModelAndView(final Step step, final String message2) {
		ModelAndView res;

		res = new ModelAndView("step/manager/edit");
		res.addObject("step", step);
		res.addObject("requestURI", "step/manager/edit.do");
		return res;
	}

}
