
package controllers.manager;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Workout;
import forms.WorkoutForm;
import services.ManagerService;
import services.WorkoutService;

@Controller
@RequestMapping("/workout/manager")
public class WorkoutManagerController extends AbstractController {

	// Services

	@Autowired
	private ManagerService	managerService;

	@Autowired
	private WorkoutService	workoutService;


	// Constructors -----------------------------------------------------------

	public WorkoutManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------



	// --------------- Creation ----------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		WorkoutForm workoutForm;
		workoutForm = new WorkoutForm();

		res = new ModelAndView("workout/manager/create");
		res.addObject("workoutForm", workoutForm);
		res.addObject("gyms", managerService.findByPrincipal().getGyms());
		res.addObject("requestURI", "workout/manager/create.do");

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "next")
	public ModelAndView next(@Valid WorkoutForm workoutForm, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = this.createCreateModelAndView(workoutForm);
		} else {
			try {
				Workout workout = workoutService.reconstruct(workoutForm);
				Workout result = workoutService.saveAndFlush(workout);
				res = new ModelAndView("redirect:/step/manager/create.do?workoutID=" + result.getId());
			} catch (Throwable oops) {
				res = this.createCreateModelAndView(workoutForm, "workout.commit.error");

			}
		}
		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid WorkoutForm workoutForm, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = this.createCreateModelAndView(workoutForm);
		} else {
			try {
				Workout workout = workoutService.reconstruct(workoutForm);
				Workout workoutGuardado = workoutService.saveAndFlush(workout);
				res = new ModelAndView("redirect:/workout/showDisplay.do?workoutId=" + workoutGuardado.getId());
			} catch (Throwable oops) {
				res = this.createCreateModelAndView(workoutForm, "workout.commit.error");

			}
		}
		return res;
	}

	// --------------- Edition ----------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int workoutID) {
		ModelAndView res;
		try{
			Workout workout = workoutService.findOne(workoutID);
			Assert.isTrue(this.workoutService.compruebaWorkout(workout));
			res = new ModelAndView("workout/manager/edit");
			res.addObject("workout", workout);
			res.addObject("gyms", managerService.findByPrincipal().getGyms());
			res.addObject("requestURI", "workout/manager/edit.do");
		}catch(Throwable oops){
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}
		
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid Workout workout, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = this.createEditModelAndView(workout);
			if(workout.getGym()==null){
				res.addObject("select","select");
			}
		} else {
			try {
				Workout workoutGuardado = workoutService.saveAndFlush(workout);
				res = new ModelAndView("redirect:/workout/showDisplay.do?workoutId=" + workoutGuardado.getId());
			} catch (Throwable oops) {
				res = this.createEditModelAndView(workout, "workout.commit.error");
				res.addObject("select","select");
			}
		}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid Workout workout, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = this.createEditModelAndView(workout);
		} else {
			try {
				int gymId = workout.getGym().getId();
				workoutService.delete(workout);
				res = new ModelAndView("redirect:/workout/listByGym.do?gymID=" + gymId);
			} catch (Throwable oops) {
				res = this.createEditModelAndView(workout, "workout.commit.error");

			}
		}
		return res;
	}

	// Ancillary methods-------------------------------------
	protected ModelAndView createCreateModelAndView(WorkoutForm workoutForm) {
		ModelAndView res;
		res = this.createCreateModelAndView(workoutForm, null);
		return res;
	}

	protected ModelAndView createCreateModelAndView(WorkoutForm workoutForm, String message2) {
		ModelAndView res;

		res = new ModelAndView("workout/manager/create");
		res.addObject("workoutForm", workoutForm);
		res.addObject("gyms", managerService.findByPrincipal().getGyms());
		res.addObject("requestURI", "workout/manager/create.do");
		return res;
	}

	protected ModelAndView createEditModelAndView(Workout workout) {
		ModelAndView res;
		res = this.createEditModelAndView(workout, null);
		return res;
	}

	protected ModelAndView createEditModelAndView(Workout workout, String message2) {
		ModelAndView res;

		res = new ModelAndView("workout/manager/edit");
		res.addObject("workout", workout);
		res.addObject("gyms", managerService.findByPrincipal().getGyms());
		res.addObject("requestURI", "workout/manager/edit.do");
		return res;
	}

}
