/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Gym;
import domain.Trainer;
import forms.ActorForm;
import services.GymService;
import services.ManagerService;
import services.TrainerService;

@Controller
@RequestMapping("/managerUri/trainer")
public class ManagerTrainerController extends AbstractController {

	// Services

	@Autowired
	private TrainerService	trainerService;

	@Autowired
	private ManagerService	managerService;

	@Autowired
	private GymService		gymService;


	// Constructors -----------------------------------------------------------

	public ManagerTrainerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// (REGISTRO) Creaci�n de un customer
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final ActorForm actorForm = new ActorForm();

		res = this.createFormModelAndView(actorForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos el nuevo customer
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final ActorForm actorForm, final BindingResult binding) {
		ModelAndView res;
		Trainer trainer;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(actorForm);
			System.out.println(binding.getAllErrors());

			if (!StringUtils.isEmpty(actorForm.getPostalAddress()) && !actorForm.getPostalAddress().matches("^(\\d{5}$)"))
				res.addObject("postal", "postal");
			if (!StringUtils.isEmpty(actorForm.getPhone()) && !actorForm.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
				res.addObject("phone", "phone");
		} else
			try {

				trainer = trainerService.reconstruct(actorForm);

				final List<String> errores = trainerService.comprobacionEditarListErrores(trainer);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(actorForm);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					trainerService.saveForm(trainer);
					res = new ModelAndView("redirect:/");
				}

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(actorForm);
				System.out.println(oops.getLocalizedMessage());
			}

		return res;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;

		result = new ModelAndView("manager/trainer/all");
		result.addObject("trainers", trainerService.findAll());
		result.addObject("requestURI", "managerUri/trainer/all.do");

		return result;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, params = "search")
	public ModelAndView search(final String keyword) {
		ModelAndView result;
		final Collection<Trainer> res;

		res = managerService.searchTrainer(keyword);

		result = new ModelAndView("manager/trainer/all");
		result.addObject("trainers", res);
		result.addObject("requestURI", "managerUri/trainer/search.do");

		return result;
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET, params = "search")
	public ModelAndView searchGet(final String keyword) {
		ModelAndView result;
		final Collection<Trainer> res;

		res = managerService.searchTrainer(keyword);

		result = new ModelAndView("manager/trainer/all");
		result.addObject("trainers", res);
		result.addObject("requestURI", "managerUri/trainer/search.do");

		return result;
	}

	@RequestMapping(value = "/searchByGym", method = RequestMethod.POST, params = "search")
	public ModelAndView searchByGym(@RequestParam final int gymID, final String keyword) {
		ModelAndView result;
		final Collection<Trainer> res;
		final Gym gym = gymService.findOne(gymID);

		res = managerService.searchTrainerByGym(gym, keyword);

		result = new ModelAndView("manager/trainer/all");
		result.addObject("trainers", res);
		result.addObject("gymID", gym.getId());
		result.addObject("requestURI", "managerUri/trainer/searchByGym.do");

		return result;
	}

	@RequestMapping(value = "/searchByGym", method = RequestMethod.GET, params = "search")
	public ModelAndView searchByGymGet(@RequestParam final int gymID, final String keyword) {
		ModelAndView result;
		final Collection<Trainer> res;
		final Gym gym = gymService.findOne(gymID);

		res = managerService.searchTrainerByGym(gym, keyword);

		result = new ModelAndView("manager/trainer/all");
		result.addObject("trainers", res);
		result.addObject("gymID", gym.getId());
		result.addObject("requestURI", "managerUri/trainer/searchByGym.do");

		return result;
	}

	//Associate Trainer To Gym

	@RequestMapping(value = "/associateTrainerToGym", method = RequestMethod.GET)
	public ModelAndView associateTrainerToGym(@RequestParam final int trainerId) {
		ModelAndView result;
		Trainer trainer;
		Collection<Gym> gyms = new ArrayList<Gym>();
		Collection<Gym> gymsAux = new ArrayList<Gym>();

		trainer = trainerService.findOne(trainerId);
		gymsAux = managerService.findByPrincipal().getGyms();
		for (Gym gym : gymsAux)
			if (!gym.getIsDeleted())
				gyms.add(gym);
		Assert.notNull(trainer);
		result = new ModelAndView("manager/trainer/associateTrainerToGym");
		result.addObject("trainer", trainer);
		result.addObject("gyms", gyms);
		result.addObject("requestURI", "managerUri/trainer/associateTrainerToGym.do");
		return result;
	}

	@RequestMapping(value = "/associateTrainerToGym", method = RequestMethod.POST, params = "save")
	public ModelAndView associateTrainerToGym(@Valid final Trainer trainer, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(trainer);
		else
			try {
				trainerService.save(trainer);
				result = new ModelAndView("redirect:/managerUri/trainer/all.do");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(trainer, "trainer.commit.error");
			}
		return result;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final ActorForm actorForm) {
		ModelAndView res;

		res = this.createFormModelAndView(actorForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final ActorForm actorForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("manager/trainer/create");
		res.addObject("actorForm", actorForm);
		res.addObject("message", message);

		return res;
	}

	protected ModelAndView createEditModelAndView(final Trainer trainer) {
		ModelAndView result;

		result = this.createEditModelAndView(trainer, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Trainer trainer, final String message) {
		ModelAndView result;
		final Collection<Gym> gyms = managerService.findByPrincipal().getGyms();
		result = new ModelAndView("manager/trainer/associateTrainerToGym");
		result.addObject("trainer", trainer);
		result.addObject("gyms", gyms);
		result.addObject("message", message);

		return result;
	}

}
