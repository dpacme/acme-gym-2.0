
package controllers.manager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Activity;
import domain.Gym;
import domain.Manager;
import domain.Trainer;
import services.ActivityService;
import services.GymService;
import services.ManagerService;
import services.TrainerService;

@Controller
@RequestMapping("/activity/manager")
public class ActivityManagerController extends AbstractController {

	// Services ---------------------------------------------------
	@Autowired
	private ActivityService	activityService;

	@Autowired
	private GymService		gymService;

	@Autowired
	private TrainerService	trainerService;

	@Autowired
	private ManagerService	managerService;


	// Constructors------------------------------------------------
	public ActivityManagerController() {
		super();
	}

	// List Method ------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Activity> activitys;
		activitys = activityService.findAll();
		result = new ModelAndView("activity/list");
		result.addObject("activitys", activitys);
		result.addObject("requestURI", "activity/manager/list.do");

		return result;
	}
	/*
	 * @RequestMapping(value = "/list", method = RequestMethod.GET)
	 * public ModelAndView list() {
	 * ModelAndView result;
	 * Collection<Activity> activitys;
	 * activitys = activityService.findAll();
	 * result = new ModelAndView("activity/list");
	 * result.addObject("activitys", activitys);
	 * result.addObject("requestURI", "activity/manager/list.do");
	 *
	 * return result;
	 * }
	 */
	// Creation --------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final Integer gymId) {
		ModelAndView result;

		final Manager manager = managerService.findByPrincipal();
		Assert.notNull(manager);

		final Gym gym = gymService.findOne(gymId);
		if (manager.getId() == gym.getManager().getId()) {
			final Activity activity = activityService.create();
			activity.setGym(gym);
			activity.setIsCanceled(false);
			result = this.createEditModelAndView(activity);

			if (gym.getIsDeleted()) {
				result = new ModelAndView("misc/error");
				result.addObject("codigoError", "error.authorization");
			} else {
				activity.setGym(gym);
				activity.setIsCanceled(false);
				result = this.createEditModelAndView(activity);
			}
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	// Edition ---------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int activityId) {
		ModelAndView result;
		Activity activity;

		final Manager manager = managerService.findByPrincipal();
		Assert.notNull(manager);

		activity = activityService.findOne(activityId);
		Assert.notNull(activity);

		if (manager.getId() == activity.getGym().getManager().getId()) {
			result = this.createEditModelAndView(activity);

			if (activity.getGym().getIsDeleted()) {
				result = new ModelAndView("misc/error");
				result.addObject("codigoError", "error.authorization");
			} else
				result = this.createEditModelAndView(activity);
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Activity activity, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(activity);
		else
			try {
				final Manager manager = managerService.findByPrincipal();
				Assert.notNull(manager);
				if (manager.getId() == activity.getGym().getManager().getId()) {
					if (activity.getStartTime().after(activity.getEndTime()) || activity.getStartTime().equals(activity.getEndTime()))
						result = this.createEditModelAndView(activity, "activity.commit.errorDate");
					else {
						activityService.save(activity);
						result = new ModelAndView("redirect:/gym/listActivities.do?gymId=" + activity.getGym().getId());
					}
				} else {
					result = new ModelAndView("misc/error");
					result.addObject("codigoError", "error.authorization");
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(activity, "activity.commit.error");
			}
		return result;
	}

	//Associate instructor

	@RequestMapping(value = "/associateTrainer", method = RequestMethod.GET)
	public ModelAndView associateTrainner(@RequestParam final int activityID) {
		ModelAndView result;

		try {
			Activity activity;
			Collection<Trainer> trainers = new ArrayList<Trainer>();

			activity = activityService.findOne(activityID);
			trainers = trainerService.findTrainers(activity);
			Assert.notNull(activity);
			if (activity.getIsCanceled()) {
				result = new ModelAndView("misc/error");
				result.addObject("codigoError", "error.authorization");
			} else {
				Assert.isTrue(activity.getGym().getManager().getId() == managerService.findByPrincipal().getId());
				result = new ModelAndView("activity/manager/associateTrainer");
				result.addObject("activity", activity);
				result.addObject("gymId", activity.getGym().getId());
				result.addObject("trainers", trainers);
				result.addObject("requestURI", "activity/manager/associateTrainer.do");
			}

		} catch (final Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}
		return result;
	}

	@RequestMapping(value = "/associateTrainer", method = RequestMethod.POST, params = "save")
	public ModelAndView associateTrainer(@Valid final Activity activity, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(activity);
		else
			try {
				activityService.save(activity);
				result = new ModelAndView("redirect:/gym/listActivities.do?gymId=" + activity.getGym().getId());
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(activity, "activity.commit.error");
			}
		return result;
	}

	// Ancillary methods -----------------------------------------
	protected ModelAndView createEditModelAndView(final Activity activity) {
		ModelAndView result;

		result = this.createEditModelAndView(activity, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Activity activity, final String message) {
		ModelAndView result;

		result = new ModelAndView("activity/edit");
		result.addObject("activity", activity);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/cancelActivity", method = RequestMethod.POST)
	public ModelAndView cancelActivity(@RequestParam final Integer gymId, @RequestParam final Integer activityId) {
		ModelAndView result;

		final Activity activity = activityService.cancelActivity(gymId, activityId);

		result = new ModelAndView("redirect:/gym/listActivities.do?gymId=" + activity.getGym().getId());

		return result;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		CustomDateEditor editor = new CustomDateEditor(new SimpleDateFormat("HH:mm"), true);
		binder.registerCustomEditor(Date.class, editor);
	}
}
