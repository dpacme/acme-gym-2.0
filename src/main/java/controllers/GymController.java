/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Gym;
import domain.Manager;
import domain.Workout;
import services.GymService;
import services.ManagerService;
import services.WorkoutService;

@Controller
@RequestMapping("/gym")
public class GymController extends AbstractController {

	// Services

	@Autowired
	private GymService		gymService;

	@Autowired
	private ManagerService	managerService;

	@Autowired
	private WorkoutService	workoutService;


	// Constructors -----------------------------------------------------------

	public GymController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	// List Method ------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Gym> gyms;
		gyms = this.gymService.getGymsNotDeleted();
		result = new ModelAndView("gym/list");
		result.addObject("gyms", gyms);
		result.addObject("requestURI", "gym/list.do");

		return result;
	}

	@RequestMapping(value = "/listTrainers", method = RequestMethod.GET)
	public ModelAndView listTrainers(@RequestParam final Integer gymId) {
		ModelAndView result;
		final Gym gym = this.gymService.findOne(gymId);
		result = new ModelAndView("trainer/list");
		result.addObject("trainers", gym.getTrainers());
		result.addObject("gymID", gym.getId());
		result.addObject("requestURI", "gym/listTrainers.do");

		return result;
	}

	@RequestMapping(value = "/listActivities", method = RequestMethod.GET)
	public ModelAndView listActivities(@RequestParam final Integer gymId) {
		ModelAndView result;
		final Gym gym = this.gymService.findOne(gymId);

		if (gym.getIsDeleted()) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		} else {
			result = new ModelAndView("activity/list");
			result.addObject("activities", gym.getActivities());
			result.addObject("requestURI", "gym/listActivities.do");
			result.addObject("gym", gym);
			final Manager manager = this.managerService.findByPrincipal();
			if (manager != null && manager.getGyms() != null && !manager.getGyms().isEmpty() && manager.getGyms().contains(gym))
				result.addObject("create", true);
			else
				result.addObject("create", false);
		}

		return result;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final Integer gymId) {
		ModelAndView result;
		final Gym gym = this.gymService.findOne(gymId);

		if (gym.getIsDeleted()) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		} else {
			result = new ModelAndView("gym/showDisplay");
			result.addObject("requestURI", "gym/show.do");
			result.addObject("gym", gym);
		}

		return result;
	}

	@RequestMapping(value = "/showByWorkout", method = RequestMethod.GET)
	public ModelAndView showByWorkout(@RequestParam final Integer workoutID) {
		ModelAndView result;
		final Workout workout = this.workoutService.findOne(workoutID);

		if (workout.getGym().getIsDeleted()) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		} else {
			result = new ModelAndView("gym/showByWorkout");
			result.addObject("requestURI", "gym/showByWorkout.do");
			result.addObject("gym", workout.getGym());
		}

		return result;
	}
}
