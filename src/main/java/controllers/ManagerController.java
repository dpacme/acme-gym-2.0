/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import domain.Manager;
import forms.ActorForm;
import services.ManagerService;

@Controller
@RequestMapping("/managerUri")
public class ManagerController extends AbstractController {

	// Services

	@Autowired
	private ManagerService managerService;


	// Constructors -----------------------------------------------------------

	public ManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// (REGISTRO) Creaci�n de un customer
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final ActorForm actorForm = new ActorForm();

		res = this.createFormModelAndView(actorForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos el nuevo customer
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final ActorForm actorForm, final BindingResult binding) {
		ModelAndView res;
		Manager manager;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(actorForm);
			System.out.println(binding.getAllErrors());
			//Errores no gestionados por binding
			if (!StringUtils.isEmpty(actorForm.getPostalAddress()) && !actorForm.getPostalAddress().matches("^(\\d{5}$)"))
				res.addObject("postal", "postal");
			if (!StringUtils.isEmpty(actorForm.getPhone()) && !actorForm.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
				res.addObject("phone", "phone");

		} else
			try {

				manager = managerService.reconstruct(actorForm);

				final List<String> errores = managerService.comprobacionEditarListErrores(manager);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(actorForm);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					managerService.saveForm(manager);
					res = new ModelAndView("redirect:/security/login.do");
				}

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(actorForm);
				System.out.println(oops.getLocalizedMessage());
			}

		return res;
	}

	// (REGISTRO) Creaci�n de un customer
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView res;

		final Collection<Manager> managers = managerService.findAll();

		res = new ModelAndView("manager/all");
		res.addObject("managers", managers);
		res.addObject("requestURI", "managerUri/all.do");
		return res;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final ActorForm actorForm) {
		ModelAndView res;

		res = this.createFormModelAndView(actorForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final ActorForm actorForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("manager/create");
		res.addObject("actorForm", actorForm);
		res.addObject("message", message);

		return res;
	}

	// (MODIFICAR DATOS) Modificar datos
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		final Manager manager = managerService.findByPrincipal();

		res = this.createFormModelAndView(manager);
		return res;
	}

	// (MODIFICAR DATOS) Guardar en la base de datos el nuevo candidate
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Manager manager, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(manager);
			System.out.println(binding.getAllErrors());
			//Errores no gestionados por binding
			if (!StringUtils.isEmpty(manager.getPostalAddress()) && !manager.getPostalAddress().matches("^(\\d{5}$)"))
				res.addObject("postal", "postal");
			if (!StringUtils.isEmpty(manager.getPhone()) && !manager.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
				res.addObject("phone", "phone");
		} else
			try {

				final List<String> errores = managerService.comprobacionEditarListErrores(manager);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(manager);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					managerService.save(manager);
					res = new ModelAndView("redirect:/");
				}

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createFormModelAndView(manager);
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect"))
					res.addObject("postal", "postal");
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct"))
					res.addObject("phone", "phone");
			}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createFormModelAndView(final Manager manager) {
		ModelAndView res;

		res = this.createFormModelAndView(manager, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final Manager manager, final String message) {
		ModelAndView res;

		res = new ModelAndView("manager/edit");
		res.addObject("manager", manager);
		res.addObject("message", message);

		return res;
	}
}
