/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Annotation;
import domain.Workout;
import services.ActorService;
import services.AnnotationService;
import services.WorkoutService;

@Controller
@RequestMapping("/annotation")
public class AnnotationController extends AbstractController {

	// Services

	@Autowired
	private AnnotationService annotationService;
	@Autowired
	private WorkoutService workoutService;
	@Autowired
	private ActorService		actorService;


	// Constructors -----------------------------------------------------------

	public AnnotationController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam int workoutID) {
		ModelAndView result;

		Workout workout = this.workoutService.findOne(workoutID);


		result = new ModelAndView("annotation/list");
		result.addObject("annotations", workout.getAnnotations());
		result.addObject("workout", workout);
		result.addObject("requestURI", "annotation/list.do");

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam int workoutID) {
		ModelAndView res;

		final Annotation annotation = this.annotationService.create(this.workoutService.findOne(workoutID));

		res = this.createModelAndView(annotation);
		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@Valid final Annotation annotation, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createModelAndView(annotation);
		else
			try {
				this.annotationService.save(annotation);
				result = new ModelAndView("redirect:/annotation/list.do?workoutID=" + annotation.getWorkout().getId());
			} catch (final Throwable oops) {
				result = this.createModelAndView(annotation, "annotation.commit.error");
			}
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int annotationID) {
		ModelAndView res;
		try {
			final Annotation annotation = this.annotationService.findOne(annotationID);
			Assert.isTrue(annotation.getActor().getId() == this.actorService.findByPrincipal().getId());
			res = this.editModelAndView(annotation);
		} catch (Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView edit(@Valid final Annotation annotation, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.editModelAndView(annotation);
		else
			try {
				this.annotationService.save(annotation);
				result = new ModelAndView("redirect:/annotation/list.do?workoutID=" + annotation.getWorkout().getId());
			} catch (final Throwable oops) {
				result = this.editModelAndView(annotation, "annotation.commit.error");
			}
		return result;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createModelAndView(final Annotation annotation) {
		ModelAndView res;

		res = this.createModelAndView(annotation, null);

		return res;
	}

	protected ModelAndView createModelAndView(final Annotation annotation, final String message) {
		ModelAndView res;

		res = new ModelAndView("annotation/create");
		res.addObject("annotation", annotation);
		res.addObject("message", message);

		return res;
	}

	protected ModelAndView editModelAndView(final Annotation annotation) {
		ModelAndView res;

		res = this.editModelAndView(annotation, null);

		return res;
	}

	protected ModelAndView editModelAndView(final Annotation annotation, final String message) {
		ModelAndView res;

		res = new ModelAndView("annotation/edit");
		res.addObject("annotation", annotation);
		res.addObject("message", message);

		return res;
	}

}
