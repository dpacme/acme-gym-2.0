/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Actor;
import services.ActorService;

@Controller
@RequestMapping("/actor")
public class ActorController extends AbstractController {

	// Services

	@Autowired
	private ActorService actorService;


	// Constructors -----------------------------------------------------------

	public ActorController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// VIEW
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam final int actorID) {
		ModelAndView result;

		final Actor actor = actorService.findOne(actorID);

		result = new ModelAndView("actor/view");
		result.addObject("actor", actor);
		result.addObject("requestURI", "actor/view.do");

		return result;
	}

}
