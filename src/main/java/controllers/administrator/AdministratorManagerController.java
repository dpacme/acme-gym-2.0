/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Manager;
import services.ManagerService;

@Controller
@RequestMapping("/administrator/manager")
public class AdministratorManagerController extends AbstractController {

	// Services

	@Autowired
	private ManagerService managerService;


	// Constructors -----------------------------------------------------------

	public AdministratorManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// Banear manager
	@RequestMapping(value = "/ban", method = RequestMethod.GET)
	public ModelAndView ban(@RequestParam final int managerId) {
		ModelAndView res;

		try {
			final Manager manager = managerService.findOne(managerId);
			managerService.ban(manager);

			res = new ModelAndView("redirect:/managerUri/all.do");
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	// Desbanear manager
	@RequestMapping(value = "/unban", method = RequestMethod.GET)
	public ModelAndView unban(@RequestParam final int managerId) {
		ModelAndView res;

		try {
			final Manager manager = managerService.findOne(managerId);
			managerService.unban(manager);

			res = new ModelAndView("redirect:/managerUri/all.do");
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

}
