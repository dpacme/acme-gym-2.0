
package controllers.administrator;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Customer;
import domain.Gym;
import domain.Workout;
import services.CustomerService;
import services.GymService;
import services.WorkoutService;

@Controller
@RequestMapping("administrator")
public class DashboardAdministratorController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	GymService		gymService;

	@Autowired
	CustomerService	customerService;

	@Autowired
	WorkoutService	workoutService;
	// Constructors -----------------------------------------------------------


	public DashboardAdministratorController() {
		super();
	}

	// Dashboard
	// ----------------------------------------------------------------

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView dashboard() {
		ModelAndView result;

		//The minimum, the maximum, the average, and the standard deviation of the number of gyms per manager.
		final Collection<Object> minMaxAvgAndStadevOfGymsPerManager = gymService.minMaxAvgAndStadevOfGymsPerManager();

		Integer minGymsPerManager = 0;
		Integer maxGymsPerManager = 0;
		Double avgGymsPerManager = 0.0;
		Double stdevGymsPerManager = 0.0;
		for (final Object o : minMaxAvgAndStadevOfGymsPerManager) {
			try {
				final Object[] x = (Object[]) o;

				if ((Integer) x[0] != null) {
					minGymsPerManager = (Integer) x[0];
				}
				if ((Integer) x[1] != null) {
					maxGymsPerManager = (Integer) x[1];
				}
				if ((Double) x[2] != null) {
					avgGymsPerManager = (Double) x[2];
				}
				if ((Double) x[3] != null) {
					stdevGymsPerManager = (Double) x[3];
				}

			} catch (final Exception e) {
				e.printStackTrace();
			}
		}

		//The minimum, the maximum, the average, and the standard deviation of the number of gyms per customer.
		Double[] minMaxAvgStdDevNumberGymsPerCustomer = customerService.minMaxAvgStdDevNumberGymsPerCustomer();
		Double minNumberGymsPerCustomer = minMaxAvgStdDevNumberGymsPerCustomer[0];
		Double maxNumberGymsPerCustomer = minMaxAvgStdDevNumberGymsPerCustomer[1];
		Double avgNumberGymsPerCustomer = minMaxAvgStdDevNumberGymsPerCustomer[2];
		Double stdevNumberGymsPerCustomer = minMaxAvgStdDevNumberGymsPerCustomer[3];

		if (minNumberGymsPerCustomer == null) {
			minNumberGymsPerCustomer = 0.0;
		}
		if (maxNumberGymsPerCustomer == null) {
			maxNumberGymsPerCustomer = 0.0;
		}
		if (avgNumberGymsPerCustomer == null) {
			avgNumberGymsPerCustomer = 0.0;
		}
		if (stdevNumberGymsPerCustomer == null) {
			stdevNumberGymsPerCustomer = 0.0;
		}



		//The minimum, the maximum, the average, and the standard deviation of the number of customers per gym.
		final Collection<Object> minMaxAvgAndStadevOfCustomersPerGym = customerService.minMaxAvgAndStadevOfCustomersPerGym();
		Integer minCustomersPerGym = 0;
		Integer maxCustomersPerGym = 0;
		Double avgCustomersPerGym = 0.0;
		Double stadevCustomersPerGym = 0.0;
		for (final Object o : minMaxAvgAndStadevOfCustomersPerGym) {
			try {
				final Object[] x = (Object[]) o;
				if ((Integer) x[0] != null) {
					minCustomersPerGym = (Integer) x[0];
				}
				if ((Integer) x[1] != null) {
					maxCustomersPerGym = (Integer) x[1];
				}
				if ((Integer) x[2] != null) {
					avgCustomersPerGym = (Double) x[2];
				}
				if ((Integer) x[3] != null) {
					stadevCustomersPerGym = (Double) x[3];
				}

			} catch (final Exception e) {
				e.printStackTrace();
			}
		}

		//The gym that offers more activities. The count must disregard activities that are cancelled.
		final Collection<Gym> getGymsWithMoreActivities = gymService.getGymsWithMoreActivities();

		//The customers who have joined more activities.
		final Collection<Customer> getCustomerWithMoreActivities = customerService.getCustomerWithMoreActivities();

		//Queries de Acme-Gym 2.0

		//The minimum, the average, and the maximum number of steps per workout.
		final Collection<Object> minMaxAvgStepsPerWorkout = workoutService.minMaxAvgStepsPerWorkout();

		Integer minStepsPerWorkout = 0;
		Integer maxStepsPerWorkout = 0;
		Double avgStepsPerWorkout = 0.0;
		for (final Object o : minMaxAvgStepsPerWorkout) {
			try {
				final Object[] x = (Object[]) o;
				if ((Integer) x[0] != null) {
					minStepsPerWorkout = (Integer) x[0];
				}
				if ((Integer) x[1] != null) {
					maxStepsPerWorkout = (Integer) x[1];
				}
				if ((Integer) x[2] != null) {
					avgStepsPerWorkout = (Double) x[2];
				}
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
		//gymService.minMaxAvgWorkoutsPerGym
		final Collection<Object> minMaxAvgWorkoutsPerGym = gymService.minMaxAvgWorkoutsPerGym();

		Integer minWorkoutsPerGym = 0;
		Integer maxWorkoutsPerGym = 0;
		Double avgWorkoutsPerGym = 0.0;
		for (final Object o : minMaxAvgWorkoutsPerGym) {
			try {
				final Object[] x = (Object[]) o;
				if ((Integer) x[0] != null) {
					minWorkoutsPerGym = (Integer) x[0];
				}
				if ((Integer) x[1] != null) {
					maxWorkoutsPerGym = (Integer) x[1];
				}
				if ((Integer) x[2] != null) {
					avgWorkoutsPerGym = (Double) x[2];
				}

			} catch (final Exception e) {
				e.printStackTrace();
			}
		}

		//
		//A listing of workouts in decreasing order number of stars.
		Collection<Workout> getWorkoutsOrderByNumberOfStars = workoutService.getWorkoutsByNumberStarsDesc();

		//*************************************************************************//
		result = new ModelAndView("administrator/dashboard");

		result.addObject("minGymsPerManager", minGymsPerManager);
		result.addObject("maxGymsPerManager", maxGymsPerManager);
		result.addObject("avgGymsPerManager", avgGymsPerManager);
		result.addObject("stdevGymsPerManager", stdevGymsPerManager);

		result.addObject("minNumberGymsPerCustomer", minNumberGymsPerCustomer);
		result.addObject("maxNumberGymsPerCustomer", maxNumberGymsPerCustomer);
		result.addObject("avgNumberGymsPerCustomer", avgNumberGymsPerCustomer);
		result.addObject("stdevNumberGymsPerCustomer", stdevNumberGymsPerCustomer);

		result.addObject("minCustomersPerGym", minCustomersPerGym);
		result.addObject("maxCustomersPerGym", maxCustomersPerGym);
		result.addObject("avgCustomersPerGym", avgCustomersPerGym);
		result.addObject("stadevCustomersPerGym", stadevCustomersPerGym);

		result.addObject("getGymsWithMoreActivities", getGymsWithMoreActivities);
		result.addObject("getCustomerWithMoreActivities", getCustomerWithMoreActivities);


		//Acme-Gym-2.0

		result.addObject("minStepsPerWorkout", minStepsPerWorkout);
		result.addObject("maxStepsPerWorkout", maxStepsPerWorkout);
		result.addObject("avgStepsPerWorkout", avgStepsPerWorkout);

		result.addObject("minWorkoutsPerGym", minWorkoutsPerGym);
		result.addObject("maxWorkoutsPerGym", maxWorkoutsPerGym);
		result.addObject("avgWorkoutsPerGym", avgWorkoutsPerGym);

		result.addObject("getWorkoutsOrderByNumberOfStars", getWorkoutsOrderByNumberOfStars);


		result.addObject("requestURI", "administrator/dashboard.do");

		return result;
	}

}
