
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Customer;
import security.UserAccount;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	@Query("select a from Customer a where a.userAccount = ?1")
	Customer findByUserAccount(UserAccount userAccount);

	@Query("select min(c.gyms.size), avg(c.gyms.size),max(c.gyms.size),stddev(c.gyms.size) from Customer c")
	Double[] minMaxAvgStdDevNumberGymsPerCustomer();

	@Query("select c from Customer c join c.activities a group by c having count(a) >= ALL (select c2.activities.size from Customer c2)")
	Collection<Customer> getCustomerWithMoreActivities();
	
	@Query("select min(m.customers.size), max(m.customers.size), avg(m.customers.size), stddev(m.customers.size) from Gym m")
	Collection<Object> minMaxAvgAndStadevOfCustomersPerGym();

}