
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Gym;

@Repository
public interface GymRepository extends JpaRepository<Gym, Integer> {

	@Query("select min(m.gyms.size), max(m.gyms.size), avg(m.gyms.size), stddev(m.gyms.size) from Manager m")
	Collection<Object> minMaxAvgAndStadevOfGymsPerManager();

	@Query("select g from Gym g where(select count(a) from Gym g2 join g2.activities a where g.id=g2.id and a.isCanceled=false) >= all(select count(a2) from Gym g3 join g3.activities a2 where a2.isCanceled=false group by g3.id)")
	Collection<Gym> getGymsWithMoreActivities();

	@Query("select g from Gym g where g.manager.id = ?1 and g.isDeleted = false")
	Collection<Gym> getGymsNotDeletedFromManager(int managerId);

	@Query("select g from Gym g where g.isDeleted = false")
	Collection<Gym> getGymsNotDeleted();

	@Query("select min(g.workouts.size), max(g.workouts.size), avg(g.workouts.size) from Gym g")
	Collection<Object> minMaxAvgWorkoutsPerGym();

}
