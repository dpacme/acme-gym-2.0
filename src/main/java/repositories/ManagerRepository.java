
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Manager;
import security.UserAccount;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Integer> {

	@Query("select a from Manager a where a.userAccount = ?1")
	Manager findByUserAccount(UserAccount userAccount);

	@Query("select cho from Manager cho where cho.userAccount.username = ?1")
	Manager findManagerByUsername(String username);
}
