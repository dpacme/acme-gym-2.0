
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Workout;

@Repository
public interface WorkoutRepository extends JpaRepository<Workout, Integer> {

	@Query("select a from Workout a where (a.title like ?1 or a.description like ?1) and a.gym.isDeleted = false and a.gym.id=?2")
	Collection<Workout> getWorkoutsByKeyword(String keyword, Integer gymId);
	
	@Query("select a from Workout a inner join a.annotations b group by a order by sum(b.numberStars) desc")
	Collection<Workout> getWorkoutsByNumberStarsDesc();
	
	@Query("select min(w.steps.size), max(w.steps.size), avg(w.steps.size) from Workout w")
	Collection<Object> minMaxAvgStepsPerWorkout();
}
