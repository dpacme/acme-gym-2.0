
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Annotation;
import domain.Workout;
import repositories.AnnotationRepository;

@Service
@Transactional
public class AnnotationService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private AnnotationRepository annotationRepository;


	// Constructor methods --------------------------------------------------------------
	public AnnotationService() {
		super();
	}

	// Supporting services --------------------------------------------------------------
	@Autowired
	private ActorService actorService;
	// Simple CRUD methods --------------------------------------------------------------

	public Annotation findOne(final int annotationId) {
		Assert.isTrue(annotationId != 0);
		Annotation result;

		result = this.annotationRepository.findOne(annotationId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Annotation> findAll() {
		Collection<Annotation> result;

		result = this.annotationRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Annotation create(Workout workout) {

		final Annotation annotation = new Annotation();
		annotation.setActor(actorService.findByPrincipal());
		annotation.setWorkout(workout);

		return annotation;
	}

	public Annotation saveAndFlush(final Annotation annotation) {
		Assert.notNull(annotation);

		return this.annotationRepository.saveAndFlush(annotation);
	}

	public void save(final Annotation annotation) {
		Assert.notNull(annotation);

		this.annotationRepository.save(annotation);
	}

	public void delete(final Annotation annotation) {
		this.annotationRepository.delete(annotation);
	}

	// Other bussines methods -----------------------------------------------------
}
