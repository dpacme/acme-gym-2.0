
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Annotation;
import domain.Gym;
import domain.Step;
import domain.Workout;
import forms.WorkoutForm;
import repositories.WorkoutRepository;

@Service
@Transactional
public class WorkoutService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private WorkoutRepository workoutRepository;


	// Constructor methods --------------------------------------------------------------
	public WorkoutService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private AnnotationService	annotationService;

	@Autowired
	private ManagerService		managerService;

	@Autowired
	private GymService			gymService;

	@Autowired
	private StepService			stepService;


	// Simple CRUD methods --------------------------------------------------------------

	public Workout findOne(final int workoutId) {
		Assert.isTrue(workoutId != 0);
		Workout result;

		result = workoutRepository.findOne(workoutId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Workout> findAll() {
		Collection<Workout> result;

		result = workoutRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Workout create() {

		final Workout workout = new Workout();

		return workout;
	}

	public Workout saveAndFlush(final Workout workout) {
		Assert.notNull(workout);
		Assert.notNull(workout.getGym());
		return workoutRepository.saveAndFlush(workout);
	}

	public void save(final Workout workout) {
		Assert.notNull(workout);

		workoutRepository.save(workout);
	}

	public void delete(final Workout workout) {
		for (final Annotation a : workout.getAnnotations()) {
			annotationService.delete(a);
		}
		workoutRepository.delete(workout);
	}

	// Other bussines methods -----------------------------------------------------

	public Workout reconstruct(final WorkoutForm workoutForm) {
		final Workout res = create();
		final Collection<Annotation> annotations = new ArrayList<Annotation>();
		final Collection<Step> steps = new ArrayList<Step>();
		final Step step = new Step();

		res.setTitle(workoutForm.getTitle());
		res.setDescription(workoutForm.getDescriptionWorkout());

		step.setDescription(workoutForm.getDescriptionStep());
		step.setTutorial(workoutForm.getTutorial());
		step.setWorkout(res);
		stepService.cambiaTutorial(step);
		steps.add(step);

		res.setAnnotations(annotations);
		res.setSteps(steps);
		res.setGym(workoutForm.getGym());

		return res;
	}

	public Boolean compruebaWorkout(Workout workout) {
		Boolean res = false;

		for (Gym g : managerService.findByPrincipal().getGyms()) {
			if (g.getWorkouts().contains(workout)) {
				res = true;
			}
		}

		return res;
	}

	public void comprueba(WorkoutForm workoutForm) {

		Assert.isTrue(workoutForm.getTitle() != "");
		Assert.isTrue(workoutForm.getDescriptionWorkout() != "");
		Assert.isTrue(workoutForm.getDescriptionStep() != "");
		Assert.isTrue(workoutForm.getTutorial() != "");
		Assert.notNull(workoutForm.getGym());
		Assert.isTrue(managerService.findByPrincipal().getGyms().contains(workoutForm.getGym()));

	}

	public void compruebaCampos(Workout workout) {

		Assert.isTrue(workout.getTitle() != "");
		Assert.isTrue(workout.getDescription() != "");
		Assert.notNull(workout.getGym());
		Assert.isTrue(managerService.findByPrincipal().getGyms().contains(workout.getGym()));

	}

	public Collection<Workout> searchWorkouts(String keyword, Integer gymId) {
		Collection<Workout> result = new ArrayList<Workout>();
		try {
			if (gymId != null) {
				result = workoutRepository.getWorkoutsByKeyword("%" + keyword + "%", gymId);
				Gym gym = gymService.findOne(gymId);
				Assert.isTrue(gym.getIsDeleted() != true);
			}

		} catch (Exception e) {
			return new ArrayList<Workout>();
		}
		return result;
	}

	//QUERY - A listing of workouts in decreasing order number of stars.
	public Collection<Workout> getWorkoutsByNumberStarsDesc() {
		return workoutRepository.getWorkoutsByNumberStarsDesc();
	}

	//QUERY - The minimum, the average, and the maximum number of steps per workout.
	public Collection<Object> minMaxAvgStepsPerWorkout() {
		try {
			Collection<Object> result;
			result = workoutRepository.minMaxAvgStepsPerWorkout();
			return result;
		} catch (final Exception e) {
			return new ArrayList<Object>();
		}
	}
}
