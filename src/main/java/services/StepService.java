
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Gym;
import domain.Step;
import domain.Workout;
import repositories.StepRepository;

@Service
@Transactional
public class StepService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private StepRepository stepRepository;


	// Constructor methods --------------------------------------------------------------
	public StepService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	@Autowired
	private ManagerService managerService;

	// Simple CRUD methods --------------------------------------------------------------

	public Step findOne(final int stepId) {
		Assert.isTrue(stepId != 0);
		Step result;

		result = this.stepRepository.findOne(stepId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Step> findAll() {
		Collection<Step> result;

		result = this.stepRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Step create(final Workout workout) {

		final Step step = new Step();
		step.setWorkout(workout);

		return step;
	}

	public Step saveAndFlush(final Step step) {
		Assert.notNull(step);
		return this.stepRepository.saveAndFlush(step);
	}

	public void save(final Step step) {
		Assert.notNull(step);

		this.stepRepository.save(step);
	}

	public void delete(final Step step) {
		this.stepRepository.delete(step);
	}

	// Other bussines methods -----------------------------------------------------

	public Boolean comprueba(Step step) {
		Boolean res = false;

		for (Gym g : this.managerService.findByPrincipal().getGyms())
			for (Workout w : g.getWorkouts())
				if (w.getSteps().contains(step))
					res = true;

		return res;
	}

	public void cambiaTutorial(Step step){
		if(!step.getTutorial().isEmpty()){
			String result ="";
			if(step.getTutorial().contains("youtube")){
				String[] substrings = step.getTutorial().split("=");
				result = "https://www.youtube.com/embed/" + substrings[1];
			} else if ((step.getTutorial().contains("vimeo"))) {
				String[] substrings = step.getTutorial().split("/");
				result = "https://player.vimeo.com/video/" + substrings[substrings.length - 1];
			}
			step.setTutorial(result);
		}
	}

}
