
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Activity;
import domain.Actor;
import domain.Gym;
import domain.Manager;
import domain.Trainer;
import forms.ActorForm;
import repositories.TrainerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class TrainerService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private TrainerRepository trainerRepository;


	// Constructor methods --------------------------------------------------------------
	public TrainerService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private ActorService	actorService;

	@Autowired
	private ManagerService	managerService;


	// Simple CRUD methods --------------------------------------------------------------

	public Trainer findOne(final int trainerId) {
		Assert.isTrue(trainerId != 0);
		Trainer result;

		result = trainerRepository.findOne(trainerId);
		Assert.notNull(result);

		return result;
	}

	public void checkIfTrainer() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.TRAINER))
				res = true;
		Assert.isTrue(res);
	}

	public Collection<Trainer> findAll() {
		Collection<Trainer> result;

		result = trainerRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Trainer trainer) {
		Assert.notNull(trainer);

		trainerRepository.save(trainer);
	}

	// Other bussines methods -----------------------------------------------------

	public Trainer findByPrincipal() {
		Trainer result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = trainerRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public Trainer reconstruct(final ActorForm a) {
		final Trainer trainer = new Trainer();
		final Collection<Activity> activities = new ArrayList<Activity>();

		final UserAccount account = new UserAccount();
		account.setPassword(a.getPassword());
		account.setUsername(a.getUsername());

		trainer.setId(0);
		trainer.setVersion(0);
		trainer.setName(a.getName());
		trainer.setSurname(a.getSurname());
		trainer.setEmail(a.getEmail());
		trainer.setPhone(a.getPhone());
		trainer.setPostalAddress(a.getPostalAddress());
		trainer.setCity(a.getCity());
		trainer.setCountry(a.getCountry());

		trainer.setUserAccount(account);

		trainer.setActivities(activities);

		Assert.isTrue(a.getPassword().equals(a.getSecondPassword()), "Passwords do not match");

		Assert.isTrue(a.getCheckBox(), "You must accept the term and conditions");

		return trainer;
	}

	public void saveForm(Trainer trainer) {

		Assert.notNull(trainer);
		String password;
		final Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		final Collection<Authority> auths = new ArrayList<>();
		final Authority auth = new Authority();
		auth.setAuthority("TRAINER");
		auths.add(auth);

		password = trainer.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		trainer.getUserAccount().setPassword(password);

		trainer.getUserAccount().setAuthorities(auths);

		trainer = trainerRepository.saveAndFlush(trainer);
	}

	public void comprobacion(final Trainer trainer) {
		if (trainer.getPostalAddress() != "")
			Assert.isTrue(trainer.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (trainer.getPhone() != "")
			Assert.isTrue(trainer.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
		for (final Actor actor : actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(trainer.getUserAccount().getUsername()));
	}

	public void comprobacionEditar(final Trainer trainer) {
		if (trainer.getPostalAddress() != "")
			Assert.isTrue(trainer.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (trainer.getPhone() != "")
			Assert.isTrue(trainer.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");

	}

	public Collection<Trainer> findTrainers(final Activity activity) {
		final Collection<Trainer> trainers = new ArrayList<Trainer>();
		final Manager manager = managerService.findByPrincipal();
		for (final Gym g : manager.getGyms())
			if (g.getId() == activity.getGym().getId())
				trainers.addAll(g.getTrainers());
		return trainers;
	}

	public List<String> comprobacionEditarListErrores(final Trainer trainer) {
		final List<String> listaErrores = new ArrayList<String>();
		if (trainer.getPostalAddress() != "" && !trainer.getPostalAddress().matches("^(\\d{5}$)"))
			listaErrores.add("postal");
		if (trainer.getPhone() != "" && !trainer.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
			listaErrores.add("phone");

		return listaErrores;
	}
}
