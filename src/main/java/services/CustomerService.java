
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Activity;
import domain.Actor;
import domain.Customer;
import domain.Gym;
import forms.ActorForm;
import repositories.CustomerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class CustomerService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private CustomerRepository customerRepository;


	// Constructor methods --------------------------------------------------------------
	public CustomerService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private ActorService actorService;


	// Simple CRUD methods --------------------------------------------------------------

	public Customer findOne(final int customerId) {
		Assert.isTrue(customerId != 0);
		Customer result;

		result = customerRepository.findOne(customerId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Customer> findAll() {
		Collection<Customer> result;

		result = customerRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Customer customer) {
		Assert.notNull(customer);

		customerRepository.save(customer);
	}

	// Other bussines methods -----------------------------------------------------

	public Customer findByPrincipal() {
		Customer result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = customerRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public void checkIfCustomer() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.CUSTOMER))
				res = true;
		Assert.isTrue(res);
	}

	public Customer reconstruct(final ActorForm a) {
		final Customer customer = new Customer();
		final Collection<Activity> activities = new ArrayList<Activity>();
		final Collection<Gym> gyms = new ArrayList<Gym>();

		final UserAccount account = new UserAccount();
		account.setPassword(a.getPassword());
		account.setUsername(a.getUsername());

		customer.setId(0);
		customer.setVersion(0);
		customer.setName(a.getName());
		customer.setSurname(a.getSurname());
		customer.setEmail(a.getEmail());
		customer.setPhone(a.getPhone());
		customer.setPostalAddress(a.getPostalAddress());
		customer.setCity(a.getCity());
		customer.setCountry(a.getCountry());

		customer.setUserAccount(account);

		customer.setActivities(activities);
		customer.setGyms(gyms);

		Assert.isTrue(a.getPassword().equals(a.getSecondPassword()), "Passwords do not match");

		Assert.isTrue(a.getCheckBox(), "You must accept the term and conditions");

		return customer;
	}

	public void saveForm(Customer customer) {

		Assert.notNull(customer);
		String password;
		final Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		final Collection<Authority> auths = new ArrayList<>();
		final Authority auth = new Authority();
		auth.setAuthority("CUSTOMER");
		auths.add(auth);

		password = customer.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		customer.getUserAccount().setPassword(password);

		customer.getUserAccount().setAuthorities(auths);

		customer = customerRepository.saveAndFlush(customer);
	}

	public void comprobacion(final Customer customer) {
		if (customer.getPostalAddress() != "")
			Assert.isTrue(customer.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (customer.getPhone() != "")
			Assert.isTrue(customer.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
		for (final Actor actor : actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(customer.getUserAccount().getUsername()));
	}

	public void comprobacionEditar(final Customer customer) {
		if (customer.getPostalAddress() != "")
			Assert.isTrue(customer.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (customer.getPhone() != "")
			Assert.isTrue(customer.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
	}

	public List<String> comprobacionEditarListErrores(final Customer customer) {
		final List<String> listaErrores = new ArrayList<String>();
		if (customer.getPostalAddress() != "" && !customer.getPostalAddress().matches("^(\\d{5}$)"))
			listaErrores.add("postal");
		if (customer.getPhone() != "" && !customer.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
			listaErrores.add("phone");

		return listaErrores;
	}

	//QUERY - The minimum, the maximum, the average, and the standard deviation of the number of gyms per customer.
	public Double[] minMaxAvgStdDevNumberGymsPerCustomer() {
		Double[] result;
		try {
			result = customerRepository.minMaxAvgStdDevNumberGymsPerCustomer();
		} catch (final Exception e) {
			result = null;
			return result;
		}
		return result;
	}

	//QUERY - The minimum, the maximum, the average, and the standard deviation of the number of customers per gym.
	public Collection<Object> minMaxAvgAndStadevOfCustomersPerGym() {
		return customerRepository.minMaxAvgAndStadevOfCustomersPerGym();
	}

	//QUERY - The customers who have joined more activities.
	public Collection<Customer> getCustomerWithMoreActivities() {
		try {
			Collection<Customer> result;
			result = customerRepository.getCustomerWithMoreActivities();
			return result;
		} catch (final Exception e) {
			return new ArrayList<Customer>();
		}
	}
}
