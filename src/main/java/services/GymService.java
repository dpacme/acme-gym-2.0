
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Activity;
import domain.Customer;
import domain.Gym;
import domain.Manager;
import domain.Trainer;
import repositories.GymRepository;

@Service
@Transactional
public class GymService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private GymRepository gymRepository;


	// Constructor methods --------------------------------------------------------------
	public GymService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private CustomerService	customerService;

	@Autowired
	private ManagerService	managerService;

	@Autowired
	private ActivityService	activityService;


	// Simple CRUD methods --------------------------------------------------------------

	public Gym findOne(int gymId) {
		Assert.isTrue(gymId != 0);
		Gym result;

		result = gymRepository.findOne(gymId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Gym> findAll() {
		Collection<Gym> result;

		result = gymRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Gym create() {

		Gym gym = new Gym();

		return gym;
	}

	public Gym saveAndFlush(Gym gym) {
		Assert.notNull(gym);

		return gymRepository.saveAndFlush(gym);
	}

	public void save(Gym gym) {
		Assert.notNull(gym);

		gymRepository.save(gym);
	}

	// Other bussines methods -----------------------------------------------------

	public Collection<Object> minMaxAvgAndStadevOfGymsPerManager() {
		return gymRepository.minMaxAvgAndStadevOfGymsPerManager();
	}

	public Collection<Gym> getGymsWithMoreActivities() {
		try {
			return gymRepository.getGymsWithMoreActivities();
		} catch (Exception e) {
			return new ArrayList<Gym>();
		}
	}

	public Gym createGym(String logo, String name, String address, Double fee) {

		Manager manager = managerService.findByPrincipal();
		Assert.notNull(manager);

		Gym gym = create();

		gym.setLogo(logo);
		gym.setName(name);
		gym.setAddress(address);
		gym.setFee(fee);
		gym.setIsDeleted(false);

		gym.setManager(manager);

		Gym gymGuardado = saveAndFlush(gym);

		Collection<Gym> gyms = manager.getGyms();

		gyms.add(gymGuardado);

		manager.setGyms(gyms);

		managerService.save(manager);

		return gymGuardado;

	}

	public void updateGym(Integer gymId, String logo, String name, String address, Double fee) {

		Manager manager = managerService.findByPrincipal();
		Assert.notNull(manager);

		Gym gym = findOne(gymId);

		Assert.isTrue(manager.getId() == gym.getManager().getId());

		gym.setLogo(logo);
		gym.setName(name);
		gym.setAddress(address);
		gym.setFee(fee);

		saveAndFlush(gym);

	}

	public Gym deleteGym(Integer gymId) {

		Manager manager = managerService.findByPrincipal();
		Assert.notNull(manager);

		Gym gym = findOne(gymId);
		Assert.isTrue(!gym.getIsDeleted());

		Assert.isTrue(manager.getId() == gym.getManager().getId());

		gym.setIsDeleted(true);

		Collection<Activity> activities = gym.getActivities();

		Collection<Activity> activitiesCanceladas = new ArrayList<>();

		if (activities != null) {
			for (Activity activity : activities) {
				activity.setIsCanceled(true);
				activitiesCanceladas.add(activity);

				Collection<Trainer> trainersActivity = activity.getTrainers();
				for (Trainer trainer : trainersActivity) {
					trainer.setActivities(null);
				}
				activity.setTrainers(null);
			}
		}

		Collection<Trainer> trainers = gym.getTrainers();

		if (trainers != null) {
			for (Trainer trainer : trainers) {
				trainer.setGym(null);
			}
		}

		Gym gymGuardado = saveAndFlush(gym);

		return gymGuardado;
	}

	public Collection<Gym> getGymsNotDeletedFromManager(int managerId) {
		return gymRepository.getGymsNotDeletedFromManager(managerId);
	}

	public Collection<Gym> getGymsNotDeleted() {
		return gymRepository.getGymsNotDeleted();
	}

	/**
	 * M�todo que a�ade el gym a la lista de gyms a los que est� apuntado
	 *
	 * @param gymId
	 * @return
	 */
	public boolean joinGym(Integer gymId) {
		boolean exito = false;

		try {
			Gym gym = findOne(gymId);
			Assert.notNull(gym);

			Customer customer = customerService.findByPrincipal();
			Assert.notNull(customer);

			if (!gym.getCustomers().contains(customer)) {
				gym.getCustomers().add(customer);
				save(gym);
				if (!customer.getGyms().contains(gym)) {
					customer.getGyms().add(gym);
					customerService.save(customer);
				}
			}
			exito = true;
		} catch (Exception e) {
			exito = false;
		}

		return exito;
	}

	/**
	 * M�todo que quita el gym a la lista de gyms a los que est� apuntado
	 *
	 * @param gymId
	 * @return
	 */
	public boolean leaveGym(Integer gymId) {
		boolean exito = false;

		try {
			Gym gym = findOne(gymId);
			Assert.notNull(gym);

			Customer customer = customerService.findByPrincipal();
			Assert.notNull(customer);

			if (gym.getCustomers().contains(customer)) {
				gym.getCustomers().remove(customer);
				save(gym);
				//Quitar al customer de todas las actividades del gym
				for (Activity activity : gym.getActivities()) {
					if (activity.getCustomers().contains(customer)) {
						activity.getCustomers().remove(customer);
						activityService.save(activity);
					}
				}
			}
			exito = true;
		} catch (Exception e) {
			exito = false;
		}

		return exito;
	}

	//QUERY - The minimum, the average, and the maximum number of workouts per gym.
	public Collection<Object> minMaxAvgWorkoutsPerGym() {
		try {
			Collection<Object> result;
			result = gymRepository.minMaxAvgWorkoutsPerGym();
			return result;
		} catch (final Exception e) {
			return new ArrayList<Object>();
		}
	}

}
