
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Actor;
import domain.Gym;
import domain.Manager;
import domain.Trainer;
import domain.Workout;
import forms.ActorForm;
import repositories.ManagerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class ManagerService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ManagerRepository managerRepository;


	// Constructor methods --------------------------------------------------------------
	public ManagerService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private TrainerService			trainerService;

	@Autowired
	private ActorService			actorService;

	@Autowired
	private AdministratorService	administratorService;


	// Simple CRUD methods --------------------------------------------------------------

	public Manager findOne(final int managerId) {
		Assert.isTrue(managerId != 0);
		Manager result;

		result = managerRepository.findOne(managerId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Manager> findAll() {
		Collection<Manager> result;

		result = managerRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Manager manager) {
		Assert.notNull(manager);

		managerRepository.save(manager);
	}

	public Manager getManagerByUsername(final String username) {
		return managerRepository.findManagerByUsername(username);
	}

	// Other bussines methods -----------------------------------------------------

	public Manager findByPrincipal() {
		Manager result;
		UserAccount userAccount;
		try {
			userAccount = LoginService.getPrincipal();
			Assert.notNull(userAccount);

			result = managerRepository.findByUserAccount(userAccount);
			Assert.notNull(result);
		} catch (final Exception e) {
			return null;
		}
		return result;
	}

	public void checkIfManager() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.MANAGER))
				res = true;
		Assert.isTrue(res);
	}

	public Manager reconstruct(final ActorForm a) {
		final Manager manager = new Manager();
		final Collection<Gym> gyms = new ArrayList<Gym>();

		final UserAccount account = new UserAccount();
		account.setPassword(a.getPassword());
		account.setUsername(a.getUsername());

		manager.setId(0);
		manager.setVersion(0);
		manager.setName(a.getName());
		manager.setSurname(a.getSurname());
		manager.setEmail(a.getEmail());
		manager.setPhone(a.getPhone());
		manager.setPostalAddress(a.getPostalAddress());
		manager.setCity(a.getCity());
		manager.setCountry(a.getCountry());
		manager.setIsBanned(false);

		manager.setUserAccount(account);

		manager.setGyms(gyms);

		Assert.isTrue(a.getPassword().equals(a.getSecondPassword()), "Passwords do not match");

		Assert.isTrue(a.getCheckBox(), "You must accept the term and conditions");

		return manager;
	}

	public void saveForm(Manager manager) {

		Assert.notNull(manager);
		String password;
		final Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		final Collection<Authority> auths = new ArrayList<>();
		final Authority auth = new Authority();
		auth.setAuthority("MANAGER");
		auths.add(auth);

		password = manager.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		manager.getUserAccount().setPassword(password);

		manager.getUserAccount().setAuthorities(auths);

		manager = managerRepository.saveAndFlush(manager);
	}

	public Collection<Trainer> searchTrainer(final String keyword) {
		final Collection<Trainer> res = trainerService.findAll();

		if (keyword != "") {
			final Collection<Trainer> conKeyword = buscadorPorKeyword(keyword);
			res.retainAll(conKeyword);
		}

		return res;
	}

	public Collection<Trainer> searchTrainerByGym(final Gym gym, final String keyword) {
		final Collection<Trainer> res = gym.getTrainers();

		if (keyword != "") {
			final Collection<Trainer> conKeyword = buscadorPorKeyword(keyword);
			res.retainAll(conKeyword);
		}

		return res;
	}

	public Collection<Trainer> buscadorPorKeyword(final String keyword) {

		final Collection<Trainer> trainers = trainerService.findAll();
		final Collection<Trainer> res = new ArrayList<Trainer>();

		for (final Trainer t : trainers)
			if (keyword != "" && (t.getName().toLowerCase().contains(keyword.toLowerCase()) || t.getSurname().toLowerCase().contains(keyword.toLowerCase())))
				res.add(t);
		return res;
	}

	public void comprobacion(final Manager manager) {
		if (manager.getPostalAddress() != "")
			Assert.isTrue(manager.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (manager.getPhone() != "")
			Assert.isTrue(manager.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
		for (final Actor actor : actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(manager.getUserAccount().getUsername()));
	}

	public void comprobacionEditar(final Manager manager) {
		if (manager.getPostalAddress() != "")
			Assert.isTrue(manager.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (manager.getPhone() != "")
			Assert.isTrue(manager.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");

	}

	public void ban(final Manager manager) {
		administratorService.checkIfAdministrator();
		manager.setIsBanned(true);
		save(manager);
	}

	public void unban(final Manager manager) {
		administratorService.checkIfAdministrator();
		manager.setIsBanned(false);
		save(manager);
	}

	public List<String> comprobacionEditarListErrores(final Manager manager) {
		final List<String> listaErrores = new ArrayList<String>();
		if (manager.getPostalAddress() != "" && !manager.getPostalAddress().matches("^(\\d{5}$)"))
			listaErrores.add("postal");
		if (manager.getPhone() != "" && !manager.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
			listaErrores.add("phone");

		return listaErrores;
	}

	public Collection<Workout> filterWorkouts(final Manager manager) {
		final Collection<Workout> res = new ArrayList<Workout>();

		for (final Gym g : manager.getGyms())
			res.addAll(g.getWorkouts());

		return res;
	}
}
