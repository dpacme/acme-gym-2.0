
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Actor;
import domain.Administrator;
import repositories.AdministratorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class AdministratorService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private AdministratorRepository	administratorRepository;

	// Supporting services --------------------------------------------------------------

	@Autowired
	private ActorService			actorService;


	// Constructor methods --------------------------------------------------------------
	public AdministratorService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Administrator findOne(final int actorId) {
		Assert.isTrue(actorId != 0);
		Administrator result;

		result = administratorRepository.findOne(actorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Administrator> findAll() {
		Collection<Administrator> result;

		result = administratorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Administrator candidate) {
		Assert.notNull(candidate);

		administratorRepository.save(candidate);
	}

	// Other bussines methods -----------------------------------------------------

	public Administrator findByPrincipal() {
		Administrator result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = administratorRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public void checkIfAdministrator() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.ADMINISTRATOR))
				res = true;
		Assert.isTrue(res);
	}

	public void comprobacion(final Administrator administrator) {
		if (administrator.getPostalAddress() != "")
			Assert.isTrue(administrator.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (administrator.getPhone() != "")
			Assert.isTrue(administrator.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
		for (final Actor actor : actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(administrator.getUserAccount().getUsername()));
	}

	public void comprobacionEditar(final Administrator administrator) {
		if (administrator.getPostalAddress() != "")
			Assert.isTrue(administrator.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (administrator.getPhone() != "")
			Assert.isTrue(administrator.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
	}

	public List<String> comprobacionEditarListErrores(final Administrator administrator) {
		final List<String> listaErrores = new ArrayList<String>();
		if (administrator.getPostalAddress() != "" && !administrator.getPostalAddress().matches("^(\\d{5}$)"))
			listaErrores.add("postal");
		if (administrator.getPhone() != "" && !administrator.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
			listaErrores.add("phone");

		return listaErrores;
	}
}
