
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Workout;

@Component
@Transactional
public class WorkoutToStringConverter implements Converter<Workout, String> {

	@Override
	public String convert(final Workout workout) {
		String res;

		if (workout == null)
			res = null;
		else
			res = String.valueOf(workout.getId());

		return res;

	}
}
