
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Annotation;

@Component
@Transactional
public class AnnotationToStringConverter implements Converter<Annotation, String> {

	@Override
	public String convert(final Annotation annotation) {
		String res;

		if (annotation == null)
			res = null;
		else
			res = String.valueOf(annotation.getId());

		return res;

	}
}
