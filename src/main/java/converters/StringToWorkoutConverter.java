
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Workout;
import repositories.WorkoutRepository;

@Component
@Transactional
public class StringToWorkoutConverter implements Converter<String, Workout> {

	@Autowired
	WorkoutRepository workoutRepository;


	@Override
	public Workout convert(final String text) {
		Workout res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.workoutRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}
}
