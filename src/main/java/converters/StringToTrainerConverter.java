
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Trainer;
import repositories.TrainerRepository;

@Component
@Transactional
public class StringToTrainerConverter implements Converter<String, Trainer> {

	@Autowired
	TrainerRepository trainerRepository;


	@Override
	public Trainer convert(final String text) {
		Trainer res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.trainerRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}

}
