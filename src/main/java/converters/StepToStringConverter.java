
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Step;

@Component
@Transactional
public class StepToStringConverter implements Converter<Step, String> {

	@Override
	public String convert(final Step step) {
		String res;

		if (step == null)
			res = null;
		else
			res = String.valueOf(step.getId());

		return res;

	}
}
