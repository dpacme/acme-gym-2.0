
package forms;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;

import domain.Gym;

public class WorkoutForm {

	// Constructor

	public WorkoutForm() {
		super();
	}


	// Atributos

	private String	title;
	private String	descriptionWorkout;
	private String	descriptionStep;
	private String	tutorial;
	private Gym		gym;


	@SafeHtml(whitelistType = WhiteListType.NONE)
	@NotEmpty
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@NotEmpty
	public String getDescriptionWorkout() {
		return this.descriptionWorkout;
	}

	public void setDescriptionWorkout(final String descriptionWorkout) {
		this.descriptionWorkout = descriptionWorkout;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@NotEmpty
	public String getDescriptionStep() {
		return this.descriptionStep;
	}

	public void setDescriptionStep(final String descriptionStep) {
		this.descriptionStep = descriptionStep;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@URL
	public String getTutorial() {
		return this.tutorial;
	}

	public void setTutorial(final String tutorial) {
		this.tutorial = tutorial;
	}

	@NotNull
	public Gym getGym() {
		return this.gym;
	}

	public void setGym(final Gym gym) {
		this.gym = gym;
	}

}
