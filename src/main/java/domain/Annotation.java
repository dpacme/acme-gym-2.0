
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

@Entity
@Access(AccessType.PROPERTY)
public class Annotation extends DomainEntity {

	private String	comment;
	private Integer	numberStars;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getComment() {
		return this.comment;
	}

	public void setComment(final String comment) {
		this.comment = comment;
	}

	@NotNull
	@Min(0)
	@Max(3)
	public Integer getNumberStars() {
		return this.numberStars;
	}

	public void setNumberStars(final Integer numberStars) {
		this.numberStars = numberStars;
	}


	// Relationships -------------------------------------------

	private Actor	actor;
	private Workout	workout;


	@Valid
	@ManyToOne(optional = false)
	public Actor getActor() {
		return this.actor;
	}

	public void setActor(final Actor actor) {
		this.actor = actor;
	}

	@Valid
	@ManyToOne(optional = false)
	public Workout getWorkout() {
		return this.workout;
	}

	public void setWorkout(final Workout workout) {
		this.workout = workout;
	}

}
