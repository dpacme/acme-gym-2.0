
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Customer extends Actor {

	// Relationships -------------------------------------------

	private Collection<Activity>	activities;
	private Collection<Gym>			gyms;


	@Valid
	@ManyToMany
	public Collection<Activity> getActivities() {
		return this.activities;
	}

	public void setActivities(final Collection<Activity> activities) {
		this.activities = activities;
	}

	@Valid
	@ManyToMany
	public Collection<Gym> getGyms() {
		return this.gyms;
	}

	public void setGyms(final Collection<Gym> gyms) {
		this.gyms = gyms;
	}

}
