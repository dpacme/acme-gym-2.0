
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
public class Step extends DomainEntity {

	private String	description;
	private String	tutorial;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@URL
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTutorial() {
		return this.tutorial;
	}

	public void setTutorial(final String tutorial) {
		this.tutorial = tutorial;
	}


	// Relationships -------------------------------------------

	private Workout workout;


	@Valid
	@ManyToOne(optional = false)
	public Workout getWorkout() {
		return this.workout;
	}

	public void setWorkout(final Workout workout) {
		this.workout = workout;
	}

}
