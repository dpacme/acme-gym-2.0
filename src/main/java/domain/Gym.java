
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
public class Gym extends DomainEntity {

	private String	logo;
	private String	address;
	private String	name;
	private Double	fee;
	private Boolean	isDeleted;


	@NotBlank
	@URL
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getLogo() {
		return this.logo;
	}

	public void setLogo(final String logo) {
		this.logo = logo;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(final String address) {
		this.address = address;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@NotNull
	@Min(0)
	public Double getFee() {
		return this.fee;
	}

	public void setFee(final Double fee) {
		this.fee = fee;
	}

	@NotNull
	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(final Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	// Relationships -------------------------------------------

	private Manager					manager;
	private Collection<Activity>	activities;
	private Collection<Trainer>		trainers;
	private Collection<Customer>	customers;
	private Collection<Workout>		workouts;


	@Valid
	@ManyToOne(optional = false)
	public Manager getManager() {
		return this.manager;
	}

	public void setManager(final Manager manager) {
		this.manager = manager;
	}

	@Valid
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "gym")
	public Collection<Activity> getActivities() {
		return this.activities;
	}

	public void setActivities(final Collection<Activity> activities) {
		this.activities = activities;
	}

	@Valid
	@OneToMany(mappedBy = "gym")
	public Collection<Trainer> getTrainers() {
		return this.trainers;
	}

	public void setTrainers(final Collection<Trainer> trainers) {
		this.trainers = trainers;
	}

	@Valid
	@ManyToMany
	public Collection<Customer> getCustomers() {
		return this.customers;
	}

	public void setCustomers(final Collection<Customer> customers) {
		this.customers = customers;
	}

	@Valid
	@OneToMany(mappedBy = "gym")
	public Collection<Workout> getWorkouts() {
		return this.workouts;
	}

	public void setWorkouts(final Collection<Workout> workouts) {
		this.workouts = workouts;
	}

}
