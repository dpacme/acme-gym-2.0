
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

@Entity
@Access(AccessType.PROPERTY)
public class Workout extends DomainEntity {

	private String	title;
	private String	description;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}


	// Relationships -------------------------------------------

	private Collection<Annotation>	annotations;
	private Collection<Step>		steps;
	private Gym						gym;


	@Valid
	@OneToMany(mappedBy = "workout")
	public Collection<Annotation> getAnnotations() {
		return this.annotations;
	}

	public void setAnnotations(final Collection<Annotation> annotations) {
		this.annotations = annotations;
	}

	@Valid
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "workout")
	public Collection<Step> getSteps() {
		return this.steps;
	}

	public void setSteps(final Collection<Step> steps) {
		this.steps = steps;
	}

	@Valid
	@ManyToOne(optional = false)
	public Gym getGym() {
		return this.gym;
	}

	public void setGym(final Gym gym) {
		this.gym = gym;
	}

}
