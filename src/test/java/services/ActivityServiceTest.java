
package services;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Activity;
import domain.Customer;
import domain.Gym;
import domain.Trainer;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ActivityServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private GymService		gymService;

	@Autowired
	private CustomerService	customerService;

	@Autowired
	private ActivityService	activityService;

	@Autowired
	private TrainerService	trainerService;

	// Templates --------------------------------------------------------------


	// An actor who is authenticated as a manager must be able to: Manage the activities in his or her gyms, which includes registering them, listing them, and cancelling them. Note that activities can't be edited or deleted.
	// Comprobamos que se crea y se cancela una activity correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto del gym y del activity.
	// Test positivo y 2 tests negativos
	@Test
	public void manageActivity() {

		List<Gym> gyms = (List<Gym>) gymService.findAll();
		Integer gymId = gyms.get(0).getId();

		final Object testingData[][] = {
			{
				"manager1", gymId, null
			}, {
				"manager1", null, NullPointerException.class
			}, {
				"manager1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++) {
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}
	protected void template(String username, Integer gymId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Activity activity = activityService.create();
			Gym gym = gymService.findOne(gymId);
			activity.setGym(gym);
			activity.setIsCanceled(false);
			activity.setDayWeek("Monday");
			activity.setDescription("DES");
			activity.setStartTime(new Date());
			activity.setEndTime(new Date());
			activity.setTitle("TITLE");
			activity.setSeatsAvailable(3);
			List<URL> urls = new ArrayList<URL>();

			urls.add(new URL("http://www.google.com"));
			activity.setPictures(urls);
			Activity activitySaved = activityService.saveAndFlush(activity);
			System.out.println("#manageActivity");
			Assert.isTrue(activitySaved != null);
			System.out.println(activitySaved.getTitle());
			Activity activityCanceled = activityService.cancelActivity(gymId, activitySaved.getId());
			Assert.isTrue(activityCanceled != null);
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: Associate a trainer who works for
	 * one of his or her gyms with an activity that is or-ganised in that gym. Note that an activity
	 * may require several trainers so that it can be delivered.
	 *
	 * En este caso se realizara la acci�n a�adir trainers a una activity.
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un manager
	 * - Los campos no son correctos
	 *
	 */
	public void associateTrainers(int activityId, String username, int trainer1Id, int trainer2Id, Class<?> expected) {

		Class<?> caught = null;

		try {

			authenticate(username);

			Trainer trainer1 = trainerService.findOne(trainer1Id);
			Assert.notNull(trainer1);
			Trainer trainer2 = trainerService.findOne(trainer2Id);

			Collection<Trainer> trainers = new ArrayList<>();

			trainers.add(trainer1);
			if (trainer2 != null) {
				trainers.add(trainer2);
			}

			Activity activity = activityService.findOne(activityId);

			activity.setTrainers(trainers);

			activityService.saveAndFlush(activity);

			unauthenticate();

		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	@Test
	public void associateTrainerDriver() {

		List<Activity> activities = (List<Activity>) activityService.findAll();

		List<Trainer> trainers = (List<Trainer>) trainerService.findAll();

		final Object testingData[][] = {
			// Borrar gym correctamente -> true
			{
				activities.get(0).getId(), "manager1", trainers.get(0).getId(), trainers.get(1).getId(), null
			}, {
				activities.get(0).getId(), "manager1", trainers.get(0).getId(), activities.get(1).getId(), IllegalArgumentException.class
			}, {
				activities.get(0).getId(), "admin", trainers.get(0).getId(), activities.get(1).getId(), IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++) {
			associateTrainers((int) testingData[i][0], (String) testingData[i][1], (int) testingData[i][2], (int) testingData[i][3], (Class<?>) testingData[i][4]);
		}
	}

	/**
	 * An actor who is not authenticated/ authenticated must be able to:
	 * Search for activities that contain a single key word in their title or descriptions and are organised on a given day at a given time.
	 *
	 * Test positivo realizando una busqueda correcta general (sin id de gym)
	 * Test negativo facilitando un id de gym (para filtrar sobre las actividades de un gym concreto) inv�lido
	 *
	 * No se realiza segundo test negativo dado que el m�todo est� preparado para que se le puedan pasar valores nulos en todos los par�metros
	 * y no hay control de roles.
	 */
	@Test
	public void searchActivities() {

		/*
		 * Se busca el primer activity que se encuentre para usar su titulo como
		 * keyword para validar que funciona el filtro
		 */
		List<Activity> activities = (List<Activity>) activityService.findAll();
		Assert.isTrue(activities != null && !activities.isEmpty());

		String keyword = activities.get(0).getTitle();

		/*
		 * Usamos el id de la activity como id de gym incorrecto
		 */
		Integer idGym = activities.get(0).getId();

		final Object testingData[][] = {
			{
				keyword, null, null
			}, {
				keyword, idGym, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template2(String keyword, Integer gymId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			List<Activity> activities = (List<Activity>) activityService.searchActivities(keyword, null, null, gymId, null);
			Assert.isTrue(activities != null && !activities.isEmpty());
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/**
	 * An actor who is not authenticated/ authenticated must be able to: Browse the catalogue of activities
	 *
	 * Dado que la acci�n la puede realizar un actor sin autenticar y autenticado y que se trata de un listado completo de actividades,
	 * solo realizaremos un test positivo donde se comprobar� que no se haya mostrado ninguna actividad invalida (donde su gym haya sido marcado como borrado)
	 */
	@Test
	public void findAllValidActivities() {

		/*
		 * Se busca el primer activity que se encuentre para usar su titulo como
		 * keyword para validar que funciona el filtro
		 */
		List<Activity> activities = (List<Activity>) activityService.findAllValid();

		Assert.isTrue(activities != null && !activities.isEmpty());

		for (Activity a : activities) {
			Assert.isTrue(a.getGym().getIsDeleted() == false);
		}
	}

	/**
	 * An actor who is authenticated as a customer must be able to: Join or leave an activity in one of the gyms that he or she's joined.
	 * Joining an activity is allowed as long as there are seats available.
	 *
	 * Test positivo apuntandose y saliendo de una actividad correctamente.
	 * Test negativo sin estar logado como customer.
	 * Test negativo con un id de gym incorrecto.
	 */
	@Test
	public void joinLeaveActivity() {

		/*
		 * Se busca un customer que est� apuntado a alg�n gym que tenga
		 * actividades a las que se pueda unir y salir.
		 * Se obtendr� el customer y la actividad a la que pueda unirse para realizar el test positivo
		 */

		List<Customer> customers = (List<Customer>) this.customerService.findAll();
		Assert.isTrue(customers != null && !customers.isEmpty());
		Customer customer = null;
		Activity activity = null;
		for (Customer c : customers) {
			if (c.getGyms() != null && !c.getGyms().isEmpty()) {
				for (Gym g : c.getGyms()) {
					if (g.getActivities() != null && !g.getActivities().isEmpty()) {
						for (Activity a : g.getActivities()) {
							if (a.getSeatsAvailable() > a.getCustomers().size()) {
								activity = a;
								customer = c;
								break;
							}
						}
					}
				}
			}
		}

		Assert.isTrue(activity != null && customer != null, "No se han encontrado customer y activity para realizar la prueba");

		/*
		 * Usamos el id del customer como id de activity incorrecto
		 */
		Integer idActivity = customer.getId();

		final Object testingData[][] = {
			{
				customer.getUserAccount().getUsername(), activity.getId(), null
			}, {
				"admin", activity.getId(), IllegalArgumentException.class
			}, {
				customer.getUserAccount().getUsername(), idActivity, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template3((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template3(String username, Integer idActivity, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			boolean exitoJoin = activityService.joinActivity(idActivity);
			boolean exitoLeave = activityService.leaveActivity(idActivity);

			Assert.isTrue(exitoJoin && exitoLeave);
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}


}

