
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Step;
import domain.Workout;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class StepServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private WorkoutService	workoutService;

	@Autowired
	private StepService		stepService;

	@Autowired
	private ManagerService	managerService;

	// Templates --------------------------------------------------------------


	/*
	 * An actor who is authenticated as a manager must be able to: Manage the workouts that the gyms that he or she manages recommends.
	 * This in-cludes listing them, creating them, editing them, and deleting them.
	 * Deleting a workout actually erases the corresponding data from the system.
	 *
	 * En este caso de uso se llevara a cabo la creacion de un step.
	 * Para forzar el error pueden darse varios casos:
	 *
	 * . El usuario logueado no es una manager
	 * . Campos incorrectos
	 * . El id del workout no existe
	 * . El workout no pertenece a un gimnasio del manager logueado
	 *
	 */
	public void createStep(final String username, final String description, final String tutorial, final Integer workoutId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			Workout w = this.workoutService.findOne(workoutId);
			Assert.isTrue(this.workoutService.compruebaWorkout(w));

			Step res = new Step();
			res.setDescription(description);
			res.setTutorial(tutorial);
			res.setWorkout(w);

			Assert.isTrue(res.getDescription() != "");

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: Manage the workouts that the gyms that he or she manages recommends.
	 * This in-cludes listing them, creating them, editing them, and deleting them.
	 * Deleting a workout actually erases the corresponding data from the system.
	 *
	 * En este caso de uso se llevara a cabo la ediciom de un step.
	 * Para forzar el error pueden darse varios casos:
	 *
	 * . El usuario logueado no es una manager
	 * . Campos incorrectos
	 * . El id del step no existe
	 *
	 */
	public void editStep(final String username, final Integer stepId, final String description, final String tutorial, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			Step res = this.stepService.findOne(stepId);
			res.setDescription(description);
			res.setTutorial(tutorial);

			Assert.isTrue(res.getDescription() != "");

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: Manage the workouts that the gyms that he or she manages recommends.
	 * This in-cludes listing them, creating them, editing them, and deleting them.
	 * Deleting a workout actually erases the corresponding data from the system.
	 *
	 * En este caso de uso se llevara a cabo el borrado de un step.
	 * Para forzar el error pueden darse varios casos:
	 *
	 * . El usuario logueado no es una manager
	 * . El id del step no existe
	 * . El step no pertenece a un workout del manager logueado
	 *
	 */
	public void deleteStep(final String username, final Integer stepId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			Step res = this.stepService.findOne(stepId);
			Assert.isTrue(this.stepService.comprueba(res));

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}


	// Drivers ----------------------------------------------------------------------

	@Test
	public void createDriver() {

		final Object testingData[][] = {
			// Create de step como admin -> false
			{
				"admin", "Description", "http://www.tutorial.com", 2777, IllegalArgumentException.class
			},
			// Create de step como customer -> false
			{
				"customer1", "Description", "http://www.tutorial.com", 2777, IllegalArgumentException.class
			},
			// Create de step como trainer -> false
			{
				"trainer1", "Description", "http://www.tutorial.com", 2777, IllegalArgumentException.class
			},
			// Create de step sin autentificar -> false
			{
				null, "Description", "http://www.tutorial.com", 2777, IllegalArgumentException.class
			},
			// Create de step como manager y con campos incorrectos -> false
			{
				"manager1", "", "http://www.tutorial.com", 2777, IllegalArgumentException.class
			},
			// El id del workout no existe -> false
			{
				"manager1", "Description", "http://www.tutorial.com", 9999, IllegalArgumentException.class
			},
			// El workout no pertenece a un gimnasio del managuer logueado -> false
			{
				"manager2", "Description", "http://www.tutorial.com", 2777, IllegalArgumentException.class
			},
			// Creacion correcta -> true
			{
				"manager1", "Description", "http://www.tutorial.com", 2777, null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.createStep((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Integer) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void editDriver() {

		final Object testingData[][] = {
			// Edit de step como admin -> false
			{
				"admin", 2778, "Description", "http://www.tutorial.com", IllegalArgumentException.class
			},
			// Edit de step como customer -> false
			{
				"customer1", 2778, "Description", "http://www.tutorial.com", IllegalArgumentException.class
			},
			// Edit de step como trainer -> false
			{
				"trainer1", 2778, "Description", "http://www.tutorial.com", IllegalArgumentException.class
			},
			// Edit de step sin autentificar -> false
			{
				null, 2778, "Description", "http://www.tutorial.com", IllegalArgumentException.class
			},
			// Edit de step como manager y con campos incorrectos -> false
			{
				"manager1", 2778, "", "http://www.tutorial.com", IllegalArgumentException.class
			},
			// El id del step no existe -> false
			{
				"manager1", 9999, "Description", "http://www.tutorial.com", IllegalArgumentException.class
			},
			// Edit correcto -> true
			{
				"manager1", 2778, "Description", "http://www.tutorial.com", null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.editStep((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void deleteDriver() {

		final Object testingData[][] = {
			// Delete de step como admin -> false
			{
				"admin", 2778, IllegalArgumentException.class
			},
			// Edit de step como customer -> false
			{
				"customer1", 2778, IllegalArgumentException.class
			},
			// Edit de step como trainer -> false
			{
				"trainer1", 2778, IllegalArgumentException.class
			},
			// Edit de step sin autentificar -> false
			{
				null, 2778, IllegalArgumentException.class
			},
			// El step no pertenece a un workout del manager logueado -> false
			{
				"manager2", 2778, IllegalArgumentException.class
			},
			// El id del step no existe -> false
			{
				"manager1", 9999, IllegalArgumentException.class
			},
			// Edit correcto -> true
			{
				"manager1", 2778, null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteStep((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
