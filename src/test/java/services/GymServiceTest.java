
package services;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Activity;
import domain.Customer;
import domain.Gym;
import domain.Manager;
import domain.Trainer;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class GymServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private GymService		gymService;

	@Autowired
	private CustomerService	customerService;

	@Autowired
	private ManagerService	managerService;

	// Templates --------------------------------------------------------------


	// An actor who is not authenticated/ authenticated must be able to: Browse the catalogue of gyms and navigate to the activities that they organise and the trainers who offer them.
	// Comprobamos que los gyms,activities y trainers se listan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativos
	@Test
	public void listGymsWithActivitiesAndTrainers() {

		List<Gym> gyms = (List<Gym>) gymService.findAll();
		Object testingData[][] = {
			{
				"", gyms.get(0).getId(), null
			}, {
				"", null, NullPointerException.class
			}, {
				"", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++) {
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	/*
	 * Manage his or her gyms, which includes registering them, listing them, editing their
	 * data, or deleting them. Deleting a gym means that its activities are automatically
	 * cancelled, that it cannot offer more activities, and that it does not appear in listings,
	 * but no data is actually removed from the system. A manager must confirm deletion
	 * twice in order for the system to proceed
	 *
	 * En este caso se realizara la acci�n de crear un gimnasio.
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un manager
	 * - Los campos no son correctos
	 *
	 */
	public void createGym(String username, String logo, String address, String name, Double fee, Class<?> expected) {

		Class<?> caught = null;

		try {

			authenticate(username);

			Manager manager = managerService.findByPrincipal();
			Assert.notNull(manager);

			gymService.createGym(logo, name, address, fee);

			unauthenticate();

		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * Manage his or her gyms, which includes registering them, listing them, editing their
	 * data, or deleting them. Deleting a gym means that its activities are automatically
	 * cancelled, that it cannot offer more activities, and that it does not appear in listings,
	 * but no data is actually removed from the system. A manager must confirm deletion
	 * twice in order for the system to proceed
	 *
	 * En este caso se realizara la acci�n de editar un gimnasio.
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un manager
	 * - El usuario logueado no es el manager poseedor el gimnasio
	 * - Los campos no son correctos
	 *
	 */
	public void editGym(int gymId, String username, String logo, String address, String name, Double fee, Class<?> expected) {

		Class<?> caught = null;

		try {

			authenticate(username);

			gymService.updateGym(gymId, logo, name, address, fee);

			unauthenticate();

		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * Manage his or her gyms, which includes registering them, listing them, editing their
	 * data, or deleting them. Deleting a gym means that its activities are automatically
	 * cancelled, that it cannot offer more activities, and that it does not appear in listings,
	 * but no data is actually removed from the system. A manager must confirm deletion
	 * twice in order for the system to proceed
	 *
	 * En este caso se realizara la acci�n de eliminar un gimnasio.
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un manager
	 * - El usuario logueado no es el manager poseedor el gimnasio
	 * - Borrar un gym ya borrado
	 *
	 */
	public void deleteGym(int gymId, String username, Class<?> expected) {

		Class<?> caught = null;

		try {

			authenticate(username);

			gymService.deleteGym(gymId);

			unauthenticate();

		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	protected void template(String username, Integer gymId, Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			Gym gym = gymService.findOne(gymId);
			System.out.println("#listGymsWithActivitiesAndTrainers");
			Assert.isTrue(gym.getActivities() != null && !gym.getActivities().isEmpty());
			for (Activity o : gym.getActivities()) {
				System.out.println(o.getTitle());
			}
			for (Trainer o : gym.getTrainers()) {
				System.out.println(o.getName());
			}

		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	@Test
	public void createDriver() {

		final Object testingData[][] = {
			// Crear gym correctamente -> true
			{
				"manager1", "http://www.imagen.com", "addressTest", "nameTest", 50.0, null
			},
			// Crear gym como no autentificado -> false
			{
				null, "http://www.imagen.com", "addressTest", "nameTest", 50.0, IllegalArgumentException.class
			},
			// Crear gym autentificado como customer-> false
			{
				"customer1", "http://www.imagen.com", "addressTest", "nameTest", 50.0, IllegalArgumentException.class
			},
			// Crear gym autentificado como admin-> false
			{
				"admin", "http://www.imagen.com", "addressTest", "nameTest", 50.0, IllegalArgumentException.class
			},
			// Crear gym con logo incorrecto -> false
			{
				"manager1", "imagen", "addressTest", "nameTest", 50.0, ConstraintViolationException.class
			},
			// Crear gym con logo vac�o -> false
			{
				"manager1", null, "addressTest", "nameTest", 50.0, ConstraintViolationException.class
			},
			// Crear gym con address vac�o -> false
			{
				"manager1", "http://www.imagen.com", null, "nameTest", 50.0, ConstraintViolationException.class
			},
			// Crear gym con name vac�o -> false
			{
				"manager1", "http://www.imagen.com", "addressTest", null, 50.0, ConstraintViolationException.class
			},
			// Crear gym con fee vac�o -> false
			{
				"manager1", "http://www.imagen.com", "addressTest", "nameTest", null, ConstraintViolationException.class
			},
			// Crear gym con fee negativo -> false
			{
				"manager1", "http://www.imagen.com", "addressTest", "nameTest", -50.0, ConstraintViolationException.class
			},

		};
		for (int i = 0; i < testingData.length; i++) {
			createGym((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Double) testingData[i][4], (Class<?>) testingData[i][5]);
		}
	}

	@Test
	public void editDriver() {

		authenticate("manager1");

		Gym gym = gymService.createGym("http://www.imagen.com", "nameTest", "addressTest", 40.0);

		Gym gym2 = gymService.createGym("http://www.imagen.com", "nameTest", "addressTest", 40.0);

		gym2 = gymService.deleteGym(gym2.getId());

		unauthenticate();

		final Object testingData[][] = {
			// Editar gym correctamente -> true
			{
				gym.getId(), "manager1", "http://www.imagen2.com", "addressTest2", "nameTest2", 80.0, null
			},
			// Editar gym con un manager diferente -> false
			{
				gym.getId(), "manager2", "http://www.imagen2.com", "addressTest2", "nameTest2", 80.0, IllegalArgumentException.class
			},
			// Editar gym con un admin -> false
			{
				gym.getId(), "admin", "http://www.imagen2.com", "addressTest2", "nameTest2", 80.0, IllegalArgumentException.class
			},
			// Editar gym con un customer -> false
			{
				gym.getId(), "customer1", "http://www.imagen2.com", "addressTest2", "nameTest2", 80.0, IllegalArgumentException.class
			},
			// Editar gym con el logo incorrecto -> false
			{
				gym.getId(), "manager1", "image", "addressTest2", "nameTest2", 80.0, ConstraintViolationException.class
			},
			// Editar gym con el logo vac�o -> false
			{
				gym.getId(), "manager1", "image", "addressTest2", "nameTest2", 80.0, ConstraintViolationException.class
			},
			// Editar gym con el address vac�o -> false
			{
				gym.getId(), "manager1", "http://www.imagen2.com", null, "nameTest2", 80.0, ConstraintViolationException.class
			},
			// Editar gym con el address vac�o -> false
			{
				gym.getId(), "manager1", "http://www.imagen2.com", "addressTest2", null, 80.0, ConstraintViolationException.class
			},
			// Editar gym con el fee vac�o -> false
			{
				gym.getId(), "manager1", "http://www.imagen2.com", "addressTest2", "nameTest2", null, ConstraintViolationException.class
			},
			// Editar gym con el fee negativo -> false
			{
				gym.getId(), "manager1", "http://www.imagen2.com", "addressTest2", "nameTest2", -80.0, ConstraintViolationException.class
			},
			// Editar gym borrado -> false
			{
				gym2.getId(), "manager1", "http://www.imagen2.com", "addressTest2", "nameTest2", -80.0, ConstraintViolationException.class
			},
		};
		for (int i = 0; i < testingData.length; i++) {
			editGym((int) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (Double) testingData[i][5], (Class<?>) testingData[i][6]);
		}
	}

	@Test
	public void deleteDriver() {

		authenticate("manager1");

		Gym gym = gymService.createGym("http://www.imagen.com", "nameTest", "addressTest", 40.0);

		Gym gym2 = gymService.createGym("http://www.imagen.com", "nameTest", "addressTest", 40.0);

		gym2 = gymService.deleteGym(gym2.getId());

		unauthenticate();

		final Object testingData[][] = {
			// Borrar gym correctamente -> true
			{
				gym.getId(), "manager1", null
			},
			// Borrar gym de otro manager -> false
			{
				gym.getId(), "manager2", IllegalArgumentException.class
			},
			// Borrar gym como admin -> false
			{
				gym.getId(), "admin", IllegalArgumentException.class
			},
			// Borrar gym como customer -> false
			{
				gym.getId(), "customer1", IllegalArgumentException.class
			},
			// Borrar gym ya borrado -> false
			{
				gym2.getId(), "manager1", IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++) {
			deleteGym((int) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	/**
	 * An actor who is authenticated as a customer must be able to: Join or leave a gym.
	 *
	 * Test positivo apuntandose y saliendo de un gym correctamente
	 * Test negativo sin estar logado como customer
	 * Test negativo con un id de gym incorrecto
	 */
	@Test
	public void joinLeaveGym() {

		/*
		 * Se busca el primer gym al que no pertenezca el primer customer encontrado
		 */

		List<Customer> customers = (List<Customer>) this.customerService.findAll();
		Assert.isTrue(customers != null && !customers.isEmpty());
		Customer customer = customers.get(0);

		List<Gym> gyms = (List<Gym>) gymService.findAll();
		Assert.isTrue(gyms != null && !gyms.isEmpty());
		gyms.removeAll(customer.getGyms());
		Assert.isTrue(gyms != null && !gyms.isEmpty());

		Gym gym = gyms.get(0);

		/*
		 * Usamos el id del customer como id de gym incorrecto
		 */
		Integer idGym = customer.getId();

		final Object testingData[][] = {
			{
				customer.getUserAccount().getUsername(), gym.getId(), null
			}, {
				"admin", gym.getId(), IllegalArgumentException.class
			}, {
				customer.getUserAccount().getUsername(), idGym, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template2(String username, Integer gymId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			boolean exitoJoin = gymService.joinGym(gymId);
			boolean exitoLeave = gymService.leaveGym(gymId);

			Assert.isTrue(exitoJoin && exitoLeave);
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

}
