
package services;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Annotation;
import domain.Gym;
import domain.Step;
import domain.Workout;
import forms.WorkoutForm;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class WorkoutServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private WorkoutService	workoutService;

	@Autowired
	private GymService		gymService;

	@Autowired
	private ManagerService	managerService;

	// Templates --------------------------------------------------------------


	// An actor who is not authenticated/ authenticated must be able to: Browse the catalogue of workouts and navigate to the activities that they organise and the trainers who offer them.
	// Comprobamos que los workouts,activities y trainers se listan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativos
	@Test
	public void listWorkoutsWithActivitiesAndTrainers() {

		final List<Workout> workouts = (List<Workout>) this.workoutService.findAll();
		final Object testingData[][] = {
			{
				"", workouts.get(0).getId(), null
			}, {
				"", null, NullPointerException.class
			}, {
				"", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template(final String username, final Integer workoutId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			final Workout workout = this.workoutService.findOne(workoutId);
			System.out.println("#listWorkoutsWithStepsAndAnnotations");
			Assert.isTrue(workout.getSteps() != null && !workout.getSteps().isEmpty());
			for (final Step o : workout.getSteps())
				System.out.println(o.getDescription());
			for (final Annotation o : workout.getAnnotations())
				System.out.println(o.getComment());

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// An actor who can display information about a gym must be able to: Search for a workout using a single key word that must appear somewhere in its title or its description.
	// Comprobamos que se puede buscar un workout por una keyword correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto del gym.
	// Test positivo y 2 tests negativos
	@Test
	public void searchWorkoutOfGym() {

		final List<Gym> gyms = (List<Gym>) this.gymService.findAll();
		final Object testingData[][] = {
			{
				"", gyms.get(0).getId(), null
			}, {
				"", null, IllegalArgumentException.class
			}, {
				"", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template2(final String username, final Integer gymId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			System.out.println("#searchWorkoutOfGym");
			final List<Workout> workouts = (List<Workout>) this.workoutService.searchWorkouts("Title", gymId);
			Assert.isTrue(workouts != null && !workouts.isEmpty());
			for (final Workout o : workouts)
				System.out.println(o.getTitle() + "-" + o.getDescription());

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: Manage the workouts that the gyms that he or she manages recommends.
	 * This in-cludes listing them, creating them, editing them, and deleting them.
	 * Deleting a workout actually erases the corresponding data from the system.
	 *
	 * En este caso de uso se llevara a cabo el listado de los workouts de un gym en concreto.
	 * Para forzar el error pueden darse varios casos:
	 *
	 * . El id del gimnasio no existe
	 *
	 */
	public void listByGym(final String username, final Integer gymId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			Gym gym = this.gymService.findOne(gymId);
			Collection<Workout> workouts = gym.getWorkouts();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: Manage the workouts that the gyms that he or she manages recommends.
	 * This in-cludes listing them, creating them, editing them, and deleting them.
	 * Deleting a workout actually erases the corresponding data from the system.
	 *
	 * En este caso de uso se llevara a cabo la creacion de un workout.
	 * Para forzar el error pueden darse varios casos:
	 *
	 * . El usuario logueado no es una manager
	 * . Campos incorrectos
	 * . El id del gimnasio no existe
	 * . El gimnasio no pertenece al manager logueado
	 *
	 */
	public void createWorkout(final String username, final String title, final String descriptionWorkout, final String descriptionStep, final String tutorial, final Integer gymId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			WorkoutForm w = new WorkoutForm();
			Gym g = this.gymService.findOne(gymId);

			w.setTitle(title);
			w.setDescriptionWorkout(descriptionWorkout);
			w.setDescriptionStep(descriptionStep);
			w.setTutorial(tutorial);
			w.setGym(g);

			this.workoutService.comprueba(w);

			Workout res = this.workoutService.reconstruct(w);

			this.workoutService.saveAndFlush(res);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: Manage the workouts that the gyms that he or she manages recommends.
	 * This in-cludes listing them, creating them, editing them, and deleting them.
	 * Deleting a workout actually erases the corresponding data from the system.
	 *
	 * En este caso de uso se llevara a cabo la edicion de un workout.
	 * Para forzar el error pueden darse varios casos:
	 *
	 * . El usuario logueado no es una manager
	 * . Campos incorrectos
	 * . El id del gimnasio no existe
	 * . El id del workout no existe
	 * . El gimnasio no pertenece al manager logueado
	 * . El workout no pertenece a un gimnasio del usuario logueado
	 *
	 *
	 */
	public void editWorkout(final String username, final Integer workoutId, final String title, final String description, final Integer gymId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			Workout w = this.workoutService.findOne(workoutId);
			Gym g = this.gymService.findOne(gymId);

			w.setTitle(title);
			w.setDescription(description);
			w.setGym(g);

			this.workoutService.compruebaCampos(w);
			Assert.isTrue(this.workoutService.compruebaWorkout(w));

			this.workoutService.saveAndFlush(w);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: Manage the workouts that the gyms that he or she manages recommends.
	 * This in-cludes listing them, creating them, editing them, and deleting them.
	 * Deleting a workout actually erases the corresponding data from the system.
	 *
	 * En este caso de uso se llevara a cabo el borrado de un workout.
	 * Para forzar el error pueden darse varios casos:
	 *
	 * . El usuario logueado no es una manager
	 * . El id del workout no existe
	 * . El workout no pertenece a un gimnasio del usuario logueado
	 *
	 *
	 */
	public void deleteWorkout(final String username, final Integer workoutId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			Workout w = this.workoutService.findOne(workoutId);
			Assert.isTrue(this.workoutService.compruebaWorkout(w));

			this.workoutService.delete(w);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void listByGymDriver() {

		final Object testingData[][] = {
			// List de workouts como admin -> true
			{
				"admin", 2756, null
			},
			// List de workouts como customer -> true
			{
				"customer1", 2756, null
			},
			// List de workouts como trainer -> true
			{
				"trainer1", 2756, null
			},
			// List de workouts sin autentificar -> true
			{
				null, 2756, null
			},
			// List de workouts como manager -> true
			{
				"manager1", 2756, null
			},
			// El id del gimnasio no existe -> false
			{
				"manager1", 99999, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listByGym((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void createDriver() {

		final Object testingData[][] = {
			// Create de workouts como admin -> false
			{
				"admin", "Title", "Description workout", "Description step", "http://www.tutorial.com", 2756, IllegalArgumentException.class
			},
			// Create de workouts como customer -> false
			{
				"customer1", "Title", "Description workout", "Description step", "http://www.tutorial.com", 2756, IllegalArgumentException.class
			},
			// Create de workouts como trainer -> false
			{
				"trainer1", "Title", "Description workout", "Description step", "http://www.tutorial.com", 2756, IllegalArgumentException.class
			},
			// Create de workouts sin autentificar -> false
			{
				null, "Title", "Description workout", "Description step", "http://www.tutorial.com", 2756, IllegalArgumentException.class
			},
			// Create de workouts como manager y con campos incorrectos -> false
			{
				"manager1", "Title", "", "Description step", "http://www.tutorial.com", 2756, IllegalArgumentException.class
			},
			// El id del gimnasio no existe -> false
			{
				"manager1", "Title", "Description workout", "Description step", "http://www.tutorial.com", 99999, IllegalArgumentException.class
			},
			// El gimnasio no pertenece al managuer logueado -> false
			{
				"manager2", "Title", "Description workout", "Description step", "http://www.tutorial.com", 2756, IllegalArgumentException.class
			},
			// Creacion correcta -> true
			{
				"manager1", "Title", "Description workout", "Description step", "http://www.tutorial.com", 2756, null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.createWorkout((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (Integer) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	@Test
	public void editDriver() {

		final Object testingData[][] = {
			// Edit de workouts como admin -> false
			{
				"admin", 2777, "Title", "Description", 2756, IllegalArgumentException.class
			},
			// Edit de workouts como customer -> false
			{
				"customer1", 2777, "Title", "Description", 2756, IllegalArgumentException.class
			},
			// Edit de workouts como trainer -> false
			{
				"trainer1", 2777, "Title", "Description", 2756, IllegalArgumentException.class
			},
			// Edit de workouts sin autentificar -> false
			{
				null, 2777, "Title", "Description", 2756, IllegalArgumentException.class
			},
			// Edit de workouts como manager y con campos incorrectos -> false
			{
				"manager1", 2777, "", "Description", 2756, IllegalArgumentException.class
			},
			// El id del gimnasio no existe -> false
			{
				"manager1", 2777, "Title", "Description", 9999, IllegalArgumentException.class
			},
			// El id del workout no existe -> false
			{
				"manager1", 9999, "Title", "Description", 2756, IllegalArgumentException.class
			},
			// El gimnasio no pertenece al managuer logueado -> false
			{
				"manager2", 2777, "", "Description", 2756, IllegalArgumentException.class
			},
			// El workout no pertenece a un gimnasio del managuer logueado -> false
			{
				"manager1", 2777, "Title", "Description", 2786, IllegalArgumentException.class
			},
			// Edit correcto -> true
			{
				"manager1", 2777, "Title", "Description", 2756, null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.editWorkout((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Integer) testingData[i][4], (Class<?>) testingData[i][5]);
	}

	@Test
	public void deleteDriver() {

		final Object testingData[][] = {
			// Delete de workouts como admin -> false
			{
				"admin", 2777, IllegalArgumentException.class
			},
			// Delete de workouts como customer -> false
			{
				"customer1", 2777, IllegalArgumentException.class
			},
			// Delete de workouts como trainer -> false
			{
				"trainer1", 2777, IllegalArgumentException.class
			},
			// Delete de workouts sin autentificar -> false
			{
				null, 2777, IllegalArgumentException.class
			},
			// El id del workout no existe -> false
			{
				"manager1", 9999, IllegalArgumentException.class
			},
			// El workout no pertenece a un gimnasio del managuer logueado -> false
			{
				"manager2", 2777, IllegalArgumentException.class
			},
			// Delete correcta -> true
			{
				"manager1", 2777, null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteWorkout((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
