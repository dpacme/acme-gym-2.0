
package services;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Activity;
import domain.Annotation;
import domain.Customer;
import domain.Gym;
import domain.Manager;
import domain.Trainer;
import domain.Workout;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class AnnotationServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private AnnotationService annotationService;

	@Autowired
	private CustomerService	customerService;

	@Autowired
	private ManagerService	managerService;

	@Autowired
	private WorkoutService	workoutService;

	// Templates --------------------------------------------------------------


	// An actor who is authenticated must be able to: Write an annotation on any workout that he or she can display.
	// Comprobamos que los gyms,activities y trainers se listan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativos
	protected void template2(String username, Integer workoutId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Workout workout = workoutService.findOne(workoutId);
			Annotation annotation = annotationService.create(workout);
			annotation.setActor(customerService.findByPrincipal());
			annotation.setComment("Comentario");
			annotation.setNumberStars(2);
			Annotation annotationSaved = this.annotationService.saveAndFlush(annotation);
			Assert.isTrue(annotationSaved != null && annotationSaved.getId() != 0);

			System.out.println(annotationSaved.getComment());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}
	
	//Drivers
	@Test
	public void writeAnnotationDriver() {

		authenticate("customer1");
		List<Workout> workouts = (List<Workout>) workoutService.findAll();
		Workout workout = workouts.get(0);
		Object testingData[][] = {
			{
				"customer1", workout.getId(), null
			}, {
				"customer1", null, NullPointerException.class
			}, {
				"customer1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++) {
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}
	

}
