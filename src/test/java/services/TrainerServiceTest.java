
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import domain.Trainer;
import forms.ActorForm;
import services.ManagerService;
import services.TrainerService;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class TrainerServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private TrainerService	trainerService;

	@Autowired
	private ManagerService	managerService;

	// Templates --------------------------------------------------------------


	/*
	 * An actor who is authenticated as a manager must be able to: Register a new trainer to the system.
	 *
	 * En este caso de uso se llevara a cabo el registro de un trainer en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario autentificado no es un manager
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerTrainer(final String username, final String name, final String surname, final String email, final String phone, final String postalAddress, final String city, final String country, final String newUsername, final String password,
		final String secondPassword, final Boolean checkBox, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			this.managerService.checkIfManager();

			// Inicializamos los atributos para la creaci�n
			final ActorForm actor = new ActorForm();

			actor.setName(name);
			actor.setSurname(surname);
			actor.setEmail(email);
			actor.setPhone(phone);
			actor.setPostalAddress(postalAddress);
			actor.setCity(city);
			actor.setCountry(country);

			actor.setUsername(newUsername);
			actor.setPassword(password);
			actor.setSecondPassword(secondPassword);

			actor.setCheckBox(checkBox);

			//Reconsturimos
			final Trainer trainer = this.trainerService.reconstruct(actor);

			//Comprobamos atributos
			this.trainerService.comprobacion(trainer);

			//Guardamos
			this.trainerService.saveForm(trainer);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: List the trainers that are registered in the system and search for them using a single key word that must be contained in their names or surnames.
	 *
	 * En este caso de uso se llevara a cabo el listado de los trainers del sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario autentificado no es un manager
	 */
	public void listTrainers(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			this.managerService.checkIfManager();

			final Collection<Trainer> trainers = this.trainerService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: List the trainers that are registered in the system and search for them using a single key word that must be contained in their names or surnames.
	 *
	 * En este caso de uso se llevara a cabo el listado de los trainers del sistema que contienen la palabra clave indicado por el usuario
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario autentificado no es un manager
	 */
	public void searchTrainers(final String username, final String keyword, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			this.managerService.checkIfManager();

			final Collection<Trainer> trainers = this.managerService.searchTrainer(keyword);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void registerTrainerDriver() {

		final Object testingData[][] = {
			// Creaci�n de trainer como autentificado (1) -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de trainer como autentificado (2) -> false
			{
				"customer1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username2", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de trainer como autentificado (3) -> false
			{
				"trainer1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username3", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de trainer como no autentificado -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username3", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de trainer con postalAddress incorrecto -> false
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "56118916511", "city", "country", "username4", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de trainer sin aceptar t�rminos -> false
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username5", "password1", "password1", false, IllegalArgumentException.class
			},
			// Creaci�n de trainer con usuario no �nico -> false
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "customer1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de trainer con contrase�as no coincidentes -> false
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username7", "password1", "password2", true, IllegalArgumentException.class
			},
			// Creaci�n de trainer con tel�fono incorrecto -> false
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "iuyvicuy", "41010", "city", "country", "username10", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con todo correcto -> true
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username8", "password1", "password1", true, null
			},
			// Creaci�n de trainer con codigopostal vacio -> true
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "", "city", "country", "username9", "password1", "password1", true, null
			},
			// Creaci�n de trainer con telefono vacio -> true
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "", "41010", "city", "country", "username13", "password1", "password1", true, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerTrainer((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (String) testingData[i][9], (String) testingData[i][10], (Boolean) testingData[i][11], (Class<?>) testingData[i][12]);
	}

	@Test
	public void listTrainerDriver() {

		final Object testingData[][] = {
			// Listado de trainers autentificado como admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			// Listado de trainers autentificado como customer -> false
			{
				"customer1", IllegalArgumentException.class
			},
			// Listado de trainers autentificado como trainer -> false
			{
				"trainer1", IllegalArgumentException.class
			},
			// Listado de trainers no autentificado -> false
			{
				null, IllegalArgumentException.class
			},
			// Listado de trainers autentificado como manager -> true
			{
				"manager1", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listTrainers((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void searchTrainerDriver() {

		final Object testingData[][] = {
			// Listado de trainers autentificado como admin -> false
			{
				"admin", "Name trainer 1", IllegalArgumentException.class
			},
			// Listado de trainers autentificado como customer -> false
			{
				"customer1", "Name trainer 1", IllegalArgumentException.class
			},
			// Listado de trainers autentificado como trainer -> false
			{
				"trainer1", "Name trainer 1", IllegalArgumentException.class
			},
			// Listado de trainers no autentificado -> false
			{
				null, "Name trainer 1", IllegalArgumentException.class
			},
			// Listado de trainers autentificado como manager -> true
			{
				"manager1", "Name trainer 1", null
			},
			// Listado de trainers autentificado como manager -> true
			{
				"manager1", "Surname trainer 1", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.searchTrainers((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
